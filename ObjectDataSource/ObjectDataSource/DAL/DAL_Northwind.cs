﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ObjectDataSource.DAL
{
    public class DAL_Northwind
    {
        /* Get all distinct countries in the Customers table */

        public List<string> getCountries()
        {
            using (var context = new NorthWindDataContext())
            {
                List<string> listOfCountries =
                    (from record in context.Customers
                     select record.Country).Distinct().ToList();

                return listOfCountries;
            }
        }

        //    Select all customers in a given country
        //    input: country
        //    output: list of customers in that country

        public List<Customer> getCustomersByCountry(string country)
        {
            using (var context = new NorthWindDataContext())
            {
                List<Customer> customers =
                    (from record in context.Customers
                     where record.Country == country
                     select record).ToList();

                return customers;
            }
        }

        // get customer by ID (ID is the primary key)
        // input: ID (string)
        // output: single Customer

        public Customer getCustomersByID(string customerID)
        {
            using (var context = new NorthWindDataContext())
            {
                Customer customer =
                    (from record in context.Customers
                     where record.CustomerID == customerID
                     select record).SingleOrDefault();

                return customer;
            }
        }

        // update customer information
        // input: customers properties / a customer obj
        // output: void

        public void updateCustomer(Customer customerInForm)
        {
            using (var context = new NorthWindDataContext())
            {
                // Making the assumption that the customer ID is not allowed to be chandeg
                Customer customerInDB = (from record in context.Customers
                                         where record.CustomerID ==                                   customerInForm.CustomerID
                                         select record).SingleOrDefault();

                if (customerInDB != null)
                {
                    customerInDB.Address = customerInForm.Address;
                    customerInDB.City = customerInForm.City;
                    customerInDB.CompanyName = customerInForm.CompanyName;
                    customerInDB.ContactName = customerInForm.ContactName;
                    customerInDB.ContactTitle = customerInForm.ContactTitle;
                    customerInDB.Country = customerInForm.Country;
                    customerInDB.Fax = customerInForm.Fax;
                    customerInDB.Phone = customerInForm.Phone;
                    customerInDB.PostalCode = customerInForm.PostalCode;
                    customerInDB.Region = customerInForm.Region;

                    context.SubmitChanges();
                }
            }
        }

        // get orders from selected date
        // input: date
        // output: list of orders

        public List<Order> getOrders(DateTime date)
        {
            using (var context = new NorthWindDataContext())
            {
                List<Order> order =
                    (from record in context.Orders
                     where record.OrderDate == date
                     select record).ToList();

                return order;
            }
        }

        // get products from specific order
        // input: date
        // output: list of products

        public List<Product> getProductName(int orderID)
        {
            
            using (var context = new NorthWindDataContext())
            {
                //List<Product> products = context.Order_Details.Join(context.Orders.Where(x => x.OrderID == orderID), od => od.OrderID, o => o.OrderID, (od, o) => od).Join(context.Products, od => od.ProductID, pr => pr.ProductID, (od, pr) => pr).ToList();

                List<Product> products =
                    (from od in context.Order_Details
                     join o in context.Orders on od.OrderID equals o.OrderID
                     join pr in context.Products on od.ProductID equals pr.ProductID
                     where o.OrderID == orderID
                     select pr).ToList();

                return products;
            }
        }
    }
}