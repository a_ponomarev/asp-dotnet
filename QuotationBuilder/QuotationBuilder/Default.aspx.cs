﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuotationBuilder
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                lblTestPostBack.Text = DateTime.Now.ToString();
            }
            else {
                lblTestPostBack.Text = "Post Back";
            }
        }

        protected void btnCalc_Click(object sender, EventArgs e)
        {
            double salesPrice = double.Parse(txtSalesPrice.Text.ToString());
            double discount = double.Parse(txtDiscPercent.Text.ToString())/100;
            double discountPrice = salesPrice * discount;
            double finalPrice = salesPrice - discountPrice;
            lblDiscount.Text = discountPrice.ToString("c");
            lblPrice.Text = finalPrice.ToString("c");
        }
    }
}