﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="QuotationBuilder.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <h1>Price Quotation</h1>
    <form id="form1" runat="server">

        <table style="width:50%">
            <tr>
                <td>Sales Price</td>
                <td>
                    <asp:TextBox ID="txtSalesPrice" runat="server" Height="22px" Width="244px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    Discount Percent</td>
                <td>
                    <br />
                    <asp:TextBox ID="txtDiscPercent" runat="server" Height="23px" Width="244px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>Discount Amount</td>
                <td>
                    <asp:Label ID="lblDiscount" runat="server" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    Total Price</td>
                <td>
                    <br />
                    <asp:Label ID="lblPrice" runat="server" Font-Bold="True"></asp:Label>
                </td>
            </tr>
        </table>

        <br />

        <asp:Button id="btnCalc" runat="server" OnClick="btnCalc_Click" Text="Calculate"></asp:Button>


    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="lblTestPostBack" runat="server" Text="Label"></asp:Label>


    </form>
</body>
</html>
