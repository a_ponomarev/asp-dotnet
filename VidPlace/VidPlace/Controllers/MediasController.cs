﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VidPlace.Models;
using VidPlace.ViewModels;
using System.Data.Entity;

namespace VidPlace.Controllers
{
    public class MediasController : Controller
    {
        //DBContext Object
        private ApplicationDbContext _context;

        public MediasController()
        {
            _context = new ApplicationDbContext();
        }

        // GET: Medias
        public ActionResult Random()
        {
            var media = new Media()
            {
                ID = 1,
                Name = "Batman"
            };

            return View(media);
        }

        // demo action results
        public ActionResult ActionResultDemo()
        {
            //return Content("Hello... Welcome to ASP dotNET MVC");
            //return HttpNotFound();
            //return new EmptyResult();
            //return Redirect("http://www.google.com");
            //return Redirect("http://localhost:56279/Medias/Random"); NOT RECOMMENDED
            //return RedirectToAction("Random");
            //return RedirectToAction("About", "Home");
            //return RedirectToAction("Random", new { userName = "Alex" });
            return RedirectToAction("About", "Home", new { page = 1, sortBy = "name" });
        }

        /*
         

        //Edit action : expects 1 input
        public ActionResult Edit(int id)
        {
            return Content("Media ID = " + id);
        }

        
        
        //Index Action: Takes 2 inputs: Page / SortBy
        public ActionResult Index(int pageIndex, string sortBy)
        {
            return Content("Page Number = " + pageIndex + " | Sort by: " + sortBy);
        }

        */

        private IEnumerable<Media> GetMedias()
        {
            return new List<Media>
            {
                new Media {ID = 1, Name="Batman"},
                new Media {ID = 2, Name="Titanic"},
                new Media {ID = 3, Name="Superman"}
            };
        }

        //Index: Full View
        public ActionResult Index(string searchString)
        {
            string viewName = "ReadOnlyList";

            if (User.IsInRole(RoleNames.CanManageMedia))
            {
                viewName = "FullList";
            }

            var medias = _context.Medias.Include(m => m.Genre);
            //var medias = GetMedias();

            if (!string.IsNullOrEmpty(searchString))
            {
                medias = medias.Where(m => m.Name.Contains(searchString));
            }

            return View(viewName, medias.ToList());
        }

        public ActionResult Details(int? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            var medias = _context.Medias.Include(m => m.Genre).Include(m => m.MediaType).SingleOrDefault(m => m.ID == id);

            if (medias == null)
            {
                return HttpNotFound();
            }

            return View(medias);
        }

        //[Route("medias/index/{pageIndex}/{sortBy}")]
        //public ActionResult Index(int? pageIndex, string sortBy)
        //{
        //    if (!pageIndex.HasValue)
        //        pageIndex = 1;
        //    if (string.IsNullOrEmpty(sortBy))
        //        sortBy = "name";

        //    return Content("Page Number = " + pageIndex + " | Sort by: " + sortBy);
        //}

        [Route("medias/released/{year:range(1900,2030)}/{month:range(1,12)}")]
        public ActionResult MediaReleased(int year, int month)
        {
            return Content("Year = " + year + " | Month = " + month);
        }

        [Route("medias/Rentals/")]
        public ActionResult listCustomers()
        {
            var media = new Media()
            {
                ID = 1,
                Name = "Superman"
            };

            var customers = new List<Customer>()
            {
                new Customer() { Name="Alex" },
                new Customer() { Name="Sally" },
                new Customer() { Name="Ben" },
                new Customer() { Name="Tom" },
            };

            var viewModel = new RentalMediaCustomerViewModel()
            {
                Media = media,
                Customers = customers
            };

            return View("Rentals", viewModel);
        }

        //Get action to load the form (1)
        [Authorize(Roles = RoleNames.CanManageMedia)]
        public ActionResult New()
        {
            var viewModel = new MediaFormViewModel
            {
                Media = new Media(),
                
                MediaType = _context.MediaTypes.ToArray(),
                Genre = _context.Genres.ToArray(),
                //ActionSelected = 0
            };

            return View("MediaForm", viewModel);
        }

        //Post action to save data from my form (2)
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Media media)
        {
            if (!ModelState.IsValid)
            {
                //Return the same form back to the user
                var viewModel = new MediaFormViewModel
                {
                    Media = media,
                    MediaType = _context.MediaTypes.ToArray(),
                    Genre = _context.Genres.ToArray(),
                };

                return View("MediaForm", viewModel);
            }

            //new media
            if (media.ID == 0)
            {
                media.DateAdded = DateTime.Now;
                _context.Medias.Add(media);
            }
            else
            {
                var mediaInDB = _context.Medias.Single(m => m.ID == media.ID);

                //Manual update
                mediaInDB.Name = media.Name;
                mediaInDB.ReleaseDate = media.ReleaseDate;
                mediaInDB.DateAdded = DateTime.Now;
                mediaInDB.MediaTypeId = media.MediaTypeId;
                mediaInDB.GenreId = media.GenreId;
                mediaInDB.NumberInStock = media.NumberInStock;
            }

            _context.SaveChanges();
            return RedirectToAction("Index", "Medias");
        }

        [NonAction]
        public Media GetMedia(int id)
        {
            return _context.Medias.SingleOrDefault(m => m.ID == id);
        }

        //Edit media details
        [Authorize(Roles = RoleNames.CanManageMedia)]
        public ActionResult Edit(int id)
        {
            var mediaInDB = _context.Medias.SingleOrDefault(m => m.ID == id);

            if (mediaInDB == null)
            {
                return HttpNotFound();
            }

            var viewModel = new MediaFormViewModel()
            {
                Media = mediaInDB,
                MediaType = _context.MediaTypes.ToList(),
                Genre = _context.Genres.ToList(),
                ActionSelected = 1
            };

            return View("MediaForm", viewModel);
        }

        ////Delete media
        //public ActionResult Delete(int id)
        //{
        //    var mediaInDB = _context.Medias.SingleOrDefault(m => m.ID == id);

        //    if (mediaInDB == null)
        //    {
        //        return HttpNotFound();
        //    }

        //    _context.Medias.Remove(mediaInDB);
        //    _context.SaveChanges();

        //    return RedirectToAction("Index");
        //}

        [Authorize(Roles = RoleNames.CanManageMedia)]
        public ActionResult Delete(int? id)
        {
            if (!id.HasValue)
                return HttpNotFound();

            var media = _context.Medias.Include(m => m.Genre).SingleOrDefault(m => m.ID == id);

            if (media == null)
                return HttpNotFound();

            return View(media);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            var media = _context.Medias.Find(id);

            _context.Medias.Remove(media);
            _context.SaveChanges();

            return RedirectToAction("Index", "Medias");
        }
    }
}