﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VidPlace.Models;

namespace VidPlace.Controllers
{
    public class HelloController : Controller
    {
        // GET: Hello
        public ActionResult Index()
        {
            return View();
        }

        // Simple Text returned to user (without layout or view)
        public string msg()
        {
            return "Hello World .. Demo 2 .. ASP.NET MVC";
        }

        // Action that returns a view with a different Name
        public ActionResult HelloWorld()
        {
            return View("Index");
        }
        
        // Action that returns a view with a different Name in the shared folder
        public ActionResult HelloShared()
        {
            return View("Error");
        }

        // Add an action that returns a customer object
        public Customer getCustomer()
        {
            Customer c = new Customer()
            {
                ID = 1,
                Name = "Alex",
            };

            return c;
        }

        // Send an object to a view
        public ActionResult helloCustomer()
        {
            Customer c = new Customer()
            {
                ID = 1,
                Name = "Alex",
            };

            ViewBag.customer = c;

            return View();
        }

        // Send an object to a view
        public ActionResult helloCustomer2()
        {
            Customer c = new Customer()
            {
                ID = 1,
                Name = "Alex",
            };

            ViewBag.customer = c;

            return View(c);
        }
    }
}