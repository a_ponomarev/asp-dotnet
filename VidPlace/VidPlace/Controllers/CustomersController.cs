﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VidPlace.Models;
using System.Data.Entity;
using VidPlace.ViewModels;

namespace VidPlace.Controllers
{
    public class CustomersController : Controller
    {
        //DBContext Object
        private ApplicationDbContext _context;

        public CustomersController()
        {
            _context = new ApplicationDbContext();
        }

        // GET: Customers
        public ActionResult Index(string searchString)
        {
            //var customers = GetCustomers();
            var customers = _context.Customers.Include(c => c.MembershipType);

            if(!string.IsNullOrEmpty(searchString))
            {
                customers = customers.Where(c => c.Name.Contains(searchString));
            }

            //var customers = _context.Customers.ToList();
            //var emptyCustomer = new List<Customer>();
            //return View(emptyCustomer);

            return View(customers.ToList());
        }

        /* Staged method to return customers to the index action */
        private IEnumerable<Customer> GetCustomers()
        {
            return new List<Customer>
            {
                new Customer {ID = 1, Name="Barry Allen"},
                new Customer {ID = 2, Name="Oliver Green"},
                new Customer {ID = 3, Name="John Smith"},
                new Customer {ID = 4, Name="Bruce Wayne"}
            };
        }

        //Details Action
        public ActionResult Details(int? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            var customer = _context.Customers.Include(c => c.MembershipType).SingleOrDefault(c => c.ID == id);
            //var customer = _context.Customers.SingleOrDefault(c => c.ID == id);
            //var customer = GetCustomers().SingleOrDefault(c => c.ID == id);

            if (customer == null)
            {
                return HttpNotFound();
            }

            return View(customer);
        }

        //Get action to load the form (1)
        public ActionResult New()
        {
            var viewModel = new CustomerFormViewModel
            {
                Customer = new Customer(),
                MembershipType = _context.MembershipTypes.ToArray()
            };

            return View("CustomerForm", viewModel);
        }

        //Post action to save data from my form (2)
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Customer customer)
        {
            //Check if the form is valid
            if (!ModelState.IsValid)
            {
                //Return the same form back to the user
                var viewModel = new CustomerFormViewModel
                {
                    Customer = customer,
                    MembershipType = _context.MembershipTypes.ToList(),
                };

                return View("CustomerForm", viewModel);
            }

            //new customer
            if(customer.ID == 0)
            {
                _context.Customers.Add(customer);
            }
            else
            {
                var customerInDB = _context.Customers.Single(c => c.ID == customer.ID);

                //TryUpdateModel(customerInDB); -- automatic cross filing function against object sent and object in the db

                //Manual update
                customerInDB.Name = customer.Name;
                customerInDB.BirthDate = customer.BirthDate;
                customerInDB.MembershipTypeId = customer.MembershipTypeId;
                customerInDB.IsSubscribedToNewsLetter = customer.IsSubscribedToNewsLetter;
            }
            
            _context.SaveChanges();
            return RedirectToAction("Index", "Customers");
        }

        //Edit customer details
        public ActionResult Edit(int id)
        {
            var customerInDB = _context.Customers.SingleOrDefault(c => c.ID == id);

            if (customerInDB == null)
            {
                return HttpNotFound();
            }

            var viewModel = new CustomerFormViewModel()
            {
                Customer = customerInDB,
                MembershipType = _context.MembershipTypes.ToList()
            };

            return View("CustomerForm", viewModel);
        }

        //This action will display a confirm msg to confirm the delete operation
        public ActionResult Delete(int? id)
        {
            if (!id.HasValue)
                return HttpNotFound();

            var customer = _context.Customers.Include(c => c.MembershipType).SingleOrDefault(c => c.ID == id);

            if (customer == null)
                return HttpNotFound();

            return View(customer);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var customer = _context.Customers.Find(id);

            _context.Customers.Remove(customer);
            _context.SaveChanges();

            return RedirectToAction("Index", "Customers");
        }
    }
}