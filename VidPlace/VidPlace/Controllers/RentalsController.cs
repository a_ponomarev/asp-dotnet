﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VidPlace.Models;
using VidPlace.ViewModels;
using System.Data.Entity;

namespace VidPlace.Controllers
{
    public class RentalsController : Controller
    {
        private ApplicationDbContext _context;

        public RentalsController()
        {
            _context = new ApplicationDbContext();
        }

        //Step 1 - Select Customer
        public ActionResult SelectCustomer(string searchString)
        {
            var customers = new List<Customer>();

            if (!string.IsNullOrEmpty(searchString))
            {
                customers = _context.Customers.Where(c => c.Name.Contains(searchString)).ToList();
            }

            return View(customers);
        }

        //Step 2 - Select Media
        public ActionResult SelectMedia(int? id, string searchString)
        {
            if(!id.HasValue)
                return RedirectToAction("SelectCustomer", "Rentals");

            var viewModel = new RentalsSelectMediaViewModel()
            {
                Customer = _context.Customers.SingleOrDefault(c => c.ID == id),
                SearchMedias = new List<Media>()
            };

            if (viewModel.Customer == null)
            {
                return RedirectToAction("SelectCustomer", "Rentals");
                //return HttpNotFound();
            }

            if (!string.IsNullOrEmpty(searchString))
            {
                viewModel.SearchMedias = _context.Medias.Where(m => m.Name.Contains(searchString)).ToList();
            }

            return View(viewModel);
        }

        //Step 3 - Rental Confirmation
        public ActionResult RentalConfirmation(int id, int mediaId)
        {
            var viewModel = new RentalConfirmationViewModel()
            {
                Customer = _context.Customers.SingleOrDefault(c => c.ID == id),
                Media = _context.Medias.SingleOrDefault(m => m.ID == mediaId)
            };

            if(viewModel.Customer == null || viewModel.Media == null)
            {
                return HttpNotFound();
            }

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult RentalConfirmation(RentalConfirmationViewModel rentalRecord)
        {
            var record = new Rental()
            {
                Customer = _context.Customers.Single(c => c.ID == rentalRecord.Customer.ID),
                Media = _context.Medias.Single(m => m.ID == rentalRecord.Media.ID),
                RentDate = DateTime.Now
            };

            _context.Rentals.Add(record);
            _context.SaveChanges();

            return RedirectToAction("Index", "Rentals");
        }

        //Step 4 - List of Available Rentals
        public ActionResult Index()
        {
            var rentalRecord = _context.Rentals.Include(c => c.Customer).Include(m => m.Media).ToList();
            return View("RentalList", rentalRecord);
        }
    }
}