﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VidPlace.Models
{
    public class MembershipType
    {
        public byte Id { get; set; }
        public string Name { get; set; }
        public short SignUpFee { get; set; }
        public byte DiscountRate { get; set; }
        public byte DurationInMonth { get; set; }

        //Check migration 201808021625519_PopulateMembershipTypes for details.
        public static readonly byte NoPlanSelected = 0;
        public static readonly byte PayAsYouGo = 1;
    }
}