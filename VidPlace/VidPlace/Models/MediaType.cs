﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VidPlace.Models
{
    public class MediaType
    {
        public short Id { get; set; }
        public string Name { get; set; }
    }
}