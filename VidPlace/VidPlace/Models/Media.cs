﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VidPlace.Models
{
    public class Media
    {
        public int ID { get; set; }

        [Required][MaxLength(100)]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Release Date")]
        public DateTime? ReleaseDate { get; set; }

        [Required]
        [Display(Name = "Date Added")]
        public DateTime? DateAdded { get; set; }

        [Required]
        [Display(Name = "Number In Stock")]
        [Range(0, 500, ErrorMessage = "The number should be between 0 and 500")]
        public short NumberInStock { get; set; }

        public Genre Genre { get; set; }
        [Display(Name = "Genre")]
        public short GenreId { get; set; }

        public MediaType MediaType { get; set; }
        [Display(Name = "Media Type")]
        public short MediaTypeId { get; set; }

        public Media()
        {
            NumberInStock = 1;
            DateAdded = DateTime.Now;
        }
    }
}