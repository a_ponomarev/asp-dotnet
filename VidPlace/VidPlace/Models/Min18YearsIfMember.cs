﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace VidPlace.Models
{
    public class Min18YearsIfMember : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var customer = (Customer)validationContext.ObjectInstance;

            //If customer chooses pay as ypu go --> no validation error
            if(customer.MembershipTypeId == MembershipType.NoPlanSelected || customer.MembershipTypeId == MembershipType.PayAsYouGo)
            {
                return ValidationResult.Success;
            }

            //Customer chooses paid plan (monthly, quarterly or annual)

            //1 - Birthdate not entered --> Validation error
            if(customer.BirthDate == null)
            {
                return new ValidationResult("Date of birth is required for paid plans");
            }

            //2 - Date is entered
            var age = DateTime.Now.Year - customer.BirthDate.Value.Year;

            //2.1 Either above 18 --> no error
            //2.2 Below 18 --> error
            return (age >= 18) ?
                ValidationResult.Success :
                new ValidationResult("Customer has to be 18 y.o. or older to get a paid plan");
        }
    }
}