﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VidPlace.Models
{
    public static class RoleNames
    {
        // This is a role that can edit, delete and add new Media records in Vidplace application

        public const string CanManageMedia = "CanManageMedia";
    }
}