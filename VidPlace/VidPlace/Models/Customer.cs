﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VidPlace.Models
{
    public class Customer
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Please enter the customer's name")][MaxLength(100)]
        public string Name { get; set; }
        public bool IsSubscribedToNewsLetter { get; set; }

        [Display(Name = "Date of Birth")][Min18YearsIfMember]
        public DateTime? BirthDate { get; set; }

        public MembershipType MembershipType { get; set; }

        [Display(Name = "Memership Type")]
        public byte MembershipTypeId { get; set; }

        //NOT NEEDED
        public override string ToString()
        {
            return this.ID + "|" + this.Name;
        }


    }
}