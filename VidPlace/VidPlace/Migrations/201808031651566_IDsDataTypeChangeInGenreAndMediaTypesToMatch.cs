namespace VidPlace.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IDsDataTypeChangeInGenreAndMediaTypesToMatch : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Media", "Genre_Id", "dbo.Genres");
            DropForeignKey("dbo.Media", "MediaType_Id", "dbo.MediaTypes");
            DropIndex("dbo.Media", new[] { "Genre_Id" });
            DropIndex("dbo.Media", new[] { "MediaType_Id" });
            RenameColumn(table: "dbo.Media", name: "Genre_Id", newName: "GenreId");
            RenameColumn(table: "dbo.Media", name: "MediaType_Id", newName: "MediaTypeId");
            DropPrimaryKey("dbo.Genres");
            DropPrimaryKey("dbo.MediaTypes");
            AlterColumn("dbo.Genres", "Id", c => c.Short(nullable: false, identity: true));
            AlterColumn("dbo.Media", "GenreId", c => c.Short(nullable: false));
            AlterColumn("dbo.Media", "MediaTypeId", c => c.Short(nullable: false));
            AlterColumn("dbo.MediaTypes", "Id", c => c.Short(nullable: false, identity: true));
            AddPrimaryKey("dbo.Genres", "Id");
            AddPrimaryKey("dbo.MediaTypes", "Id");
            CreateIndex("dbo.Media", "GenreId");
            CreateIndex("dbo.Media", "MediaTypeId");
            AddForeignKey("dbo.Media", "GenreId", "dbo.Genres", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Media", "MediaTypeId", "dbo.MediaTypes", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Media", "MediaTypeId", "dbo.MediaTypes");
            DropForeignKey("dbo.Media", "GenreId", "dbo.Genres");
            DropIndex("dbo.Media", new[] { "MediaTypeId" });
            DropIndex("dbo.Media", new[] { "GenreId" });
            DropPrimaryKey("dbo.MediaTypes");
            DropPrimaryKey("dbo.Genres");
            AlterColumn("dbo.MediaTypes", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Media", "MediaTypeId", c => c.Int());
            AlterColumn("dbo.Media", "GenreId", c => c.Int());
            AlterColumn("dbo.Genres", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.MediaTypes", "Id");
            AddPrimaryKey("dbo.Genres", "Id");
            RenameColumn(table: "dbo.Media", name: "MediaTypeId", newName: "MediaType_Id");
            RenameColumn(table: "dbo.Media", name: "GenreId", newName: "Genre_Id");
            CreateIndex("dbo.Media", "MediaType_Id");
            CreateIndex("dbo.Media", "Genre_Id");
            AddForeignKey("dbo.Media", "MediaType_Id", "dbo.MediaTypes", "Id");
            AddForeignKey("dbo.Media", "Genre_Id", "dbo.Genres", "Id");
        }
    }
}
