namespace VidPlace.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDefaultUserAccounts : DbMigration
    {
        public override void Up()
        {
            Sql(@"

INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'b597ea07-37f1-41f9-a833-fc8c4bc140d7', N'admin@vidplace.com', 0, N'AN/+2d24HqRFuYqkbNB3SrW06LnqQVu1HAB6XAfjD0fmdCTYIvyntX15kFo1USeAqg==', N'f0832210-c621-4011-a9b8-93849d6e2de2', NULL, 0, 0, NULL, 1, 0, N'admin@vidplace.com')
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'c5b9efa3-6096-4eb9-b90d-ad0d29e4e61d', N'guest@vidplace.com', 0, N'AOXQElYp6SpLlkHW0F5tFTsHXU65DMrhU1oO0W/O+AwKibfkNLdkGdNpF+8oPn1d9Q==', N'9a9dbe0f-80a1-4167-8ac2-dbb7850d862e', NULL, 0, 0, NULL, 1, 0, N'guest@vidplace.com')

INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'd75eedd7-6389-46af-978a-ce22cda3d4c3', N'CanManageMedia')

INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'b597ea07-37f1-41f9-a833-fc8c4bc140d7', N'd75eedd7-6389-46af-978a-ce22cda3d4c3')

");
        }
        
        public override void Down()
        {
        }
    }
}
