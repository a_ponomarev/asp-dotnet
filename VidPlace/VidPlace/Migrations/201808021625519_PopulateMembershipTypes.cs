namespace VidPlace.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateMembershipTypes : DbMigration
    {
        public override void Up()
        {
            //Add logic to populate MembershipType table with my business logic
            Sql("INSERT INTO MembershipTypes " +
                "(Id, Name, SignUpFee, DurationInMonth, DiscountRate)" +
                " Values(1, 'Pay as you go', 0, 0, 0)");

            Sql("INSERT INTO MembershipTypes " +
                "(Id, Name, SignUpFee, DurationInMonth, DiscountRate)" +
                " Values(2, 'Monthly', 10, 1, 10)");

            Sql("INSERT INTO MembershipTypes " +
                "(Id, Name, SignUpFee, DurationInMonth, DiscountRate)" +
                " Values(3, 'Quarterly', 30, 4, 15)");

            Sql("INSERT INTO MembershipTypes " +
                "(Id, Name, SignUpFee, DurationInMonth, DiscountRate)" +
                " Values(4, 'Annual', 100, 12, 20)");
        }
        
        public override void Down()
        {
        }
    }
}
