namespace VidPlace.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPropertiesToMedia : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Media", "ReleaseDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Media", "DateAdded", c => c.DateTime(nullable: false));
            AddColumn("dbo.Media", "NumberInStock", c => c.Short(nullable: false));
            AddColumn("dbo.Media", "Genre_Id", c => c.Int());
            AddColumn("dbo.Media", "MediaType_Id", c => c.Int());
            AlterColumn("dbo.Media", "Name", c => c.String(nullable: false, maxLength: 100));
            CreateIndex("dbo.Media", "Genre_Id");
            CreateIndex("dbo.Media", "MediaType_Id");
            AddForeignKey("dbo.Media", "Genre_Id", "dbo.Genres", "Id");
            AddForeignKey("dbo.Media", "MediaType_Id", "dbo.MediaTypes", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Media", "MediaType_Id", "dbo.MediaTypes");
            DropForeignKey("dbo.Media", "Genre_Id", "dbo.Genres");
            DropIndex("dbo.Media", new[] { "MediaType_Id" });
            DropIndex("dbo.Media", new[] { "Genre_Id" });
            AlterColumn("dbo.Media", "Name", c => c.String());
            DropColumn("dbo.Media", "MediaType_Id");
            DropColumn("dbo.Media", "Genre_Id");
            DropColumn("dbo.Media", "NumberInStock");
            DropColumn("dbo.Media", "DateAdded");
            DropColumn("dbo.Media", "ReleaseDate");
        }
    }
}
