namespace VidPlace.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingRentals : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Rentals",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RentDate = c.DateTime(nullable: false),
                        ReturnDate = c.DateTime(),
                        Customer_ID = c.Int(nullable: false),
                        Media_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Customers", t => t.Customer_ID, cascadeDelete: true)
                .ForeignKey("dbo.Media", t => t.Media_ID, cascadeDelete: true)
                .Index(t => t.Customer_ID)
                .Index(t => t.Media_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Rentals", "Media_ID", "dbo.Media");
            DropForeignKey("dbo.Rentals", "Customer_ID", "dbo.Customers");
            DropIndex("dbo.Rentals", new[] { "Media_ID" });
            DropIndex("dbo.Rentals", new[] { "Customer_ID" });
            DropTable("dbo.Rentals");
        }
    }
}
