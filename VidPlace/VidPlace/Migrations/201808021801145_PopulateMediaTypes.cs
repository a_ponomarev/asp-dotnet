namespace VidPlace.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class PopulateMediaTypes : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO MediaTypes " +
                " Values('Movie')");

            Sql("INSERT INTO MediaTypes " +
                " Values('TV Show')");

            Sql("INSERT INTO MediaTypes " +
                " Values('Tutorial')");
        }

        public override void Down()
        {
        }
    }
}
