﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace VidPlace
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapMvcAttributeRoutes();
            /*
             
            // Custom route for index - medias
            routes.MapRoute(
                name: "media-index",
                url: "medias/index/{pageIndex}/{sortBy}",
                defaults: new { controller = "Medias", action = "Index" }
            );

            routes.MapRoute(
                name: "media-released",
                url: "medias/releaseDate/{year}/{month}",   // @@@@> Значение releaseDate может быть любое, главное чтобы action соответсовали с существующим action
                defaults: new { controller = "Medias", action = "MediaReleased" },
                constraints: new {year = @"2017|2018", month = @"^(0?[1-9]|1[012])$"}
            );

            //constraints: new { year = @"\d{4}", month = @"^(0?[1-9]|1[012])$" };
            
            */

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
