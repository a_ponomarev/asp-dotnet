﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VidPlace.Models;

namespace VidPlace.ViewModels
{
    public class RentalsSelectMediaViewModel
    {
        public Customer Customer { get; set; }
        public IEnumerable<Media> SearchMedias { get; set; }
    }
}