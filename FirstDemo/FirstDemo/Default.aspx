﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="FirstDemo.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            Hello World<br />
            <br />
            Welcome to ASP dotNET<br />
            <br />
            <br />
        </div>
        <asp:Label ID="lblDate" runat="server"></asp:Label>
    </form>
</body>
</html>
