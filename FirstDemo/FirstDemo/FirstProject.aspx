﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FirstProject.aspx.cs" Inherits="FirstDemo.FirstProject" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>First Project</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="lblName" runat="server" Font-Bold="True" Text="Name:"></asp:Label>

            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

            <asp:TextBox ID="txtName" runat="server" Height="16px" Width="234px"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="btnSubmit" runat="server" Height="20px" Text="Submit" Width="312px" OnClick="btnSubmit_Click" />
            <br />
            <br />
            <asp:Label ID="lblUserEntry" runat="server" Font-Bold="True"></asp:Label>
        </div>
    </form>
</body>
</html>
