﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FirstDemo
{
    public partial class FirstProject : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (txtName.Text == string.Empty)
            {
                lblUserEntry.ForeColor = System.Drawing.Color.Red;
                lblUserEntry.Text = "Please enter a name...";
            }
            else
            {
                String msg = "Hello, ";
                msg += txtName.Text;
                lblUserEntry.Text = msg;
            }

        }
    }
}