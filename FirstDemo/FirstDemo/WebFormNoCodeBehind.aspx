﻿<%@ Page Language="C#" AutoEventWireup="true"  %>

<!DOCTYPE html>
<script src="Scripts/JavaScript.js"></script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="lblDate" runat="server"></asp:Label>
        </div>
    </form>
</body>
    <script lang="c#" runat="server">
        protected void Page_Load(object sender, EventArgs e)
        {
            lblDate.Text = DateTime.Now.ToString();
        }
    </script>
</html>
