﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VideoRentalStore__Assignment1_
{
    public partial class NewCustomerTarget : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            NewCustomer prevPage = (NewCustomer)Page.PreviousPage;

            if (prevPage != null)
            {
                lblFN.Text = prevPage.name;
                lblLN.Text = prevPage.lastName;
                lblAddress.Text = prevPage.address;
                lblPhone.Text = prevPage.phone;
            }
        }
    }
}