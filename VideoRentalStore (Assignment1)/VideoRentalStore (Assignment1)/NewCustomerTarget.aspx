﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="NewCustomerTarget.aspx.cs" Inherits="VideoRentalStore__Assignment1_.NewCustomerTarget" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            text-align: left;
        }
        .auto-style3 {
            text-align: left;
            width: 120px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <p>
        You have entered the following information:</p>
    <table class="auto-style1">
        <tr>
            <td class="auto-style3">First Name:</td>
            <td class="auto-style2">
                <asp:Label ID="lblFN" runat="server" Font-Bold="True" Font-Italic="True" ForeColor="#FF3300"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">Last Name:</td>
            <td class="auto-style2">
                <asp:Label ID="lblLN" runat="server" Font-Bold="True" Font-Italic="True" ForeColor="#FF3300"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">Address:</td>
            <td class="auto-style2">
                <asp:Label ID="lblAddress" runat="server" Font-Bold="True" Font-Italic="True" ForeColor="#FF3300"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">Phone:</td>
            <td class="auto-style2">
                <asp:Label ID="lblPhone" runat="server" Font-Bold="True" Font-Italic="True" ForeColor="#FF3300"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
