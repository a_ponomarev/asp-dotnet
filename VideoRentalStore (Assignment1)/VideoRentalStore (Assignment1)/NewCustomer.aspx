﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="NewCustomer.aspx.cs" Inherits="VideoRentalStore__Assignment1_.NewCustomer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            text-align: center;
        }
        .auto-style3 {
            width: 134px;
            text-align: left;
        }
        .auto-style4 {
            text-align: left;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    
    <table class="auto-style1">
        <tr>
            <td class="auto-style2" colspan="2"><strong>New Customer</strong></td>
        </tr>
        <tr>
            <td class="auto-style2" colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style4" colspan="2">
                <asp:ValidationSummary ID="vsErrors" runat="server" Font-Bold="True" Font-Italic="True" ForeColor="Red" />
            </td>
        </tr>
        <tr>
            <td class="auto-style2" colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style3">First Name:</td>
            <td class="auto-style4">
                <asp:TextBox ID="txtFirstName" runat="server" Width="200px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvFN" runat="server" ControlToValidate="txtFirstName" Display="Dynamic" ErrorMessage="First Name is required" Font-Bold="True" ForeColor="Red">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">Last Name:</td>
            <td class="auto-style4">
                <asp:TextBox ID="txtLastName" runat="server" Width="200px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvLN" runat="server" ControlToValidate="txtLastName" Display="Dynamic" ErrorMessage="Last Name is required" Font-Bold="True" ForeColor="Red">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">Address:</td>
            <td class="auto-style4">
                <asp:TextBox ID="txtAddress" runat="server" Width="200px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvAddress" runat="server" ControlToValidate="txtAddress" Display="Dynamic" ErrorMessage="Address is required" Font-Bold="True" ForeColor="Red">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">Phone:</td>
            <td class="auto-style4">
                <asp:TextBox ID="txtPhone" runat="server" Width="200px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvPhone" runat="server" ControlToValidate="txtPhone" Display="Dynamic" ErrorMessage="Phone is required" Font-Bold="True" ForeColor="Red">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="regexvPhone" runat="server" ControlToValidate="txtPhone" ErrorMessage="Please input telephone in following format:  514-000-0000" ForeColor="Red" ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}">*</asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">&nbsp;</td>
            <td class="auto-style4">
                <asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_Click" Text="Add Customer" />
            </td>
        </tr>
    </table>
    
</asp:Content>
