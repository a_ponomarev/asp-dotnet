namespace TimeTracker.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using TimeTracker.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<TimeTracker.Models.TimeTrackerDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(TimeTracker.Models.TimeTrackerDBContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            // Remove old TEST RECORDS FROM DB - For test purposes ONLY
            context.TimeCards.RemoveRange(context.TimeCards);
            context.Employee.RemoveRange(context.Employee);

            // Add new records again
            List<Employee> employees = new List<Employee>();

            employees.Add(new Employee()
            {
                // ID = 1
                FirstName = "Alex",
                LastName = "Dan",
                department = "IT",
                Role = "IT Support",
                DateOfHire = DateTime.Now.AddDays(-7),
                Email = "hi@hi.com",
                timeCards = new List<TimeCard>
                {
                    new TimeCard
                    {
                        submissionDate = DateTime.Now,
                        MondayHours = 8,
                        TuesdayHours = 7,
                        WednesdayHours = 6,
                        ThursdayHours = 10,
                        FridayHours = 5,
                        SaturdayHours = 0,
                        SundayHours = 0
                    },

                    new TimeCard
                    {
                        submissionDate = DateTime.Now.AddDays(-7),
                        MondayHours = 7,
                        TuesdayHours = 8,
                        WednesdayHours = 5,
                        ThursdayHours = 7,
                        FridayHours = 8,
                        SaturdayHours = 0,
                        SundayHours = 0
                    }
                }
            });

            employees.Add(new Employee()
            {
                // ID = 2
                FirstName = "Shushan",
                LastName = "Lan",
                department = "Management",
                Role = "CEO",
                DateOfHire = DateTime.Now.AddDays(-365),
                Email = "bye@bye.com",
                timeCards = new List<TimeCard>
                {
                    new TimeCard
                    {
                        submissionDate = DateTime.Now,
                        MondayHours = 5,
                        TuesdayHours = 5,
                        WednesdayHours = 6,
                        ThursdayHours = 8,
                        FridayHours = 5,
                        SaturdayHours = 5,
                        SundayHours = 2
                    },

                    new TimeCard
                    {
                        submissionDate = DateTime.Now.AddDays(-7),
                        MondayHours = 7,
                        TuesdayHours = 8,
                        WednesdayHours = 5,
                        ThursdayHours = 7,
                        FridayHours = 8,
                        SaturdayHours = 0,
                        SundayHours = 0
                    }
                }
            });

            context.Employee.AddRange(employees);
            base.Seed(context);
        }
    }
}
