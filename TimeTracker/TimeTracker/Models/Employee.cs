﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace TimeTracker.Models
{
    public class Employee
    {
        public int ID { get; set; }

        [Required][StringLength(100)]
        public string FirstName { get; set; }

        [Required][StringLength(100)]
        public string LastName { get; set; }

        [Required][StringLength(10)]
        public string department { get; set; }

        public List<TimeCard> timeCards { get; set; }

        // new prop
        [StringLength(25)]
        public string Role { get; set; }

        public DateTime DateOfHire { get; set; }

        [EmailAddress]
        public string Email { get; set; }

    }
}