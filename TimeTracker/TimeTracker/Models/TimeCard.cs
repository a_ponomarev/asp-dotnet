﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TimeTracker.Models
{
    public class TimeCard
    {
        public int ID { get; set; }
        public DateTime submissionDate { get; set; }

        [Range(0,16)]
        public int MondayHours { get; set; }

        [Range(0, 16)]
        public int TuesdayHours { get; set; }

        [Range(0, 16)]
        public int WednesdayHours { get; set; }

        [Range(0, 16)]
        public int ThursdayHours { get; set; }

        [Range(0, 16)]
        public int FridayHours { get; set; }

        [Range(0, 16)]
        public int SaturdayHours { get; set; }

        [Range(0, 16)]
        public int SundayHours { get; set; }
    }
}