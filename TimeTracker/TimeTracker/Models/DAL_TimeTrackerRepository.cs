﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
// 1 - Add namespace
using TimeTracker.Models;

namespace TimeTracker
{
    public class DAL_TimeTrackerRepository
    {
        // 2 - create a context
        TimeTrackerDBContext context = new TimeTrackerDBContext();

        // get all employees 

        public List<Employee> getAllEmployees() {
            return (from e in context.Employee
                    select e).ToList();
        }

        // get all timecards

        public List<TimeCard> getAllTimeCardsForEmployee(int empID)
        {
            return (from e in context.Employee
                    where e.ID == empID
                    select e.timeCards).SingleOrDefault();
        }

    }
}