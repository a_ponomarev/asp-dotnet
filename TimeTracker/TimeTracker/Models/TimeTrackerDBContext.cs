﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

// 1. Add name space
using System.Data.Entity;

namespace TimeTracker.Models
{
    // 2. Inherit from DBcontext (add " : DbContext")

    public class TimeTrackerDBContext : DbContext
    {
        // 3. Add DB sets for DB table
        public DbSet<Employee> Employee { get; set; }
        public DbSet<TimeCard> TimeCards { get; set; }
    }
}