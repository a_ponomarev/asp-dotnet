﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Phono__Assignment_3_.Startup))]
namespace Phono__Assignment_3_
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
