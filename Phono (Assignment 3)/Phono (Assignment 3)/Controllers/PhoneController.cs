﻿using Phono__Assignment_3_.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace Phono__Assignment_3_.Controllers
{
    public class PhoneController : Controller
    {
        //DBContext Object
        private ApplicationDbContext _context;

        public PhoneController()
        {
            _context = new ApplicationDbContext();
        }

        // GET: Phone
        public ActionResult Index()
        {

            var phones = _context.Phones.ToList();

            return View(phones);
        }

        //Details Action
        public ActionResult Details(int? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            var phone = _context.Phones.Include(p => p.Brand).Include(p => p.PhoneType).SingleOrDefault(p => p.Id == id);

            if (phone == null)
            {
                return HttpNotFound();
            }

            return View(phone);
        }
    }
}