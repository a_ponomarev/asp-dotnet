﻿using Phono__Assignment_3_.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Phono__Assignment_3_.ViewModels;

namespace Phono__Assignment_3_.Controllers
{
    public class BrandsController : Controller
    {
        //DBContext Object
        private ApplicationDbContext _context;

        public BrandsController()
        {
            _context = new ApplicationDbContext();
        }

        // GET: Brands
        public ActionResult Index()
        {
            var brands = _context.Brands.ToList();

            return View(brands);
        }

        public ActionResult AvailablePhones(int? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            var phonesOfTheBrand = new AvailablePhonesViewModel
            {
                Phones = _context.Phones.Include(p => p.Brand).Where(p => p.Brand.Id == id).ToList(),
                Brand = _context.Brands.FirstOrDefault(b => b.Id == id)
            };

            if (phonesOfTheBrand == null || phonesOfTheBrand.Brand == null)
            {
                return HttpNotFound();
            }

            return View("Phones", phonesOfTheBrand);
        }
    }
}