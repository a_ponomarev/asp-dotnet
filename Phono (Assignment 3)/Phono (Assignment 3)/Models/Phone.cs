﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phono__Assignment_3_.Models
{
    public class Phone
    {
        public short Id { get; set; }

        public string PhoneName { get; set; }

        public Brand Brand { get; set; }
        public short BrandId { get; set; }

        public DateTime? DateReleased { get; set; }

        public string ScreenSize { get; set; }

        public PhoneType PhoneType { get; set; }
        public short PhoneTypeId { get; set; }

    }
}