﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phono__Assignment_3_.Models
{
    public class PhoneType
    {
        public short Id { get; set; }

        public string Type { get; set; }

        public string OS { get; set; }
    }
}