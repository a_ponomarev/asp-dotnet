﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phono__Assignment_3_.Models
{
    public class Brand
    {
        public short Id { get; set; }

        public string BrandName { get; set; }

        public string CountryOfOrigin { get; set; }
    }
}