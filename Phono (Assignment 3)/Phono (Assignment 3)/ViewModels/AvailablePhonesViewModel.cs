﻿using Phono__Assignment_3_.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phono__Assignment_3_.ViewModels
{
    public class AvailablePhonesViewModel
    {
        public string BrandName { get; set; }
        public Brand Brand { get; set; }
        public IEnumerable<Phone> Phones { get; set; }
    }
}