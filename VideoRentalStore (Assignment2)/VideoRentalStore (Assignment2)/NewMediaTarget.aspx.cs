﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VideoRentalStore__Assignment2_
{
    public partial class NewMediaTarget : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            NewMedia prevPage = (NewMedia)Page.PreviousPage;

            if (prevPage != null)
            {
                lblTitle.Text = prevPage.title;
                lblType.Text = prevPage.type;
                lblYear.Text = prevPage.year;
            }
        }
    }
}