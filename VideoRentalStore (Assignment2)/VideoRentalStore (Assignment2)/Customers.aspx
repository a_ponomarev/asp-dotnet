﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Customers.aspx.cs" Inherits="VideoRentalStore__Assignment1_.Customers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style2 {
            text-align: left;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div style="width:50%; margin:0 auto" class="auto-style1">
        <div class="auto-style2">
        <br />
        
        </div>
        
        <div class="auto-style2">
        
        <asp:GridView ID="gvCustomers" runat="server" AutoGenerateColumns="False" CellPadding="4" DataSourceID="ObjDSGetAllCustomers" ForeColor="#333333" GridLines="None" style="margin:0 auto" DataKeyNames="ID" OnSelectedIndexChanged="gvCustomers_SelectedIndexChanged">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:CommandField ShowSelectButton="True" />
                <asp:BoundField DataField="ID" HeaderText="ID" SortExpression="ID" />
                <asp:BoundField DataField="FirstName" HeaderText="FirstName" SortExpression="FirstName" />
                <asp:BoundField DataField="LastName" HeaderText="LastName" SortExpression="LastName" />
                <asp:BoundField DataField="Address" HeaderText="Address" SortExpression="Address" />
                <asp:BoundField DataField="Phone" HeaderText="Phone" SortExpression="Phone" />
            </Columns>
            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
            <SortedAscendingCellStyle BackColor="#FDF5AC" />
            <SortedAscendingHeaderStyle BackColor="#4D0000" />
            <SortedDescendingCellStyle BackColor="#FCF6C0" />
            <SortedDescendingHeaderStyle BackColor="#820000" />
        </asp:GridView>
        </div>
        <asp:ObjectDataSource ID="ObjDSGetAllCustomers" runat="server" SelectMethod="getAllCustomers" TypeName="VideoRentalStore__Assignment2_.Models.DAL_VideoRentalStoreRepository"></asp:ObjectDataSource>
        <br />
        <asp:DetailsView ID="dvCustomer" runat="server" AutoGenerateRows="False" DataSourceID="ObjDSGetCustomerByID" style="margin:0 auto; width: 50%">
            <Fields>
                <asp:BoundField DataField="ID" HeaderText="ID" SortExpression="ID" />
                <asp:BoundField DataField="FirstName" HeaderText="FirstName" SortExpression="FirstName" />
                <asp:BoundField DataField="LastName" HeaderText="LastName" SortExpression="LastName" />
                <asp:BoundField DataField="Address" HeaderText="Address" SortExpression="Address" />
                <asp:BoundField DataField="Phone" HeaderText="Phone" SortExpression="Phone" />
                <asp:CommandField ShowEditButton="True" />
            </Fields>
        </asp:DetailsView>
        <asp:ObjectDataSource ID="ObjDSGetCustomerByID" runat="server" DataObjectTypeName="VideoRentalStore__Assignment2_.Models.Customer" SelectMethod="getCustomerByID" TypeName="VideoRentalStore__Assignment2_.Models.DAL_VideoRentalStoreRepository" UpdateMethod="updateCustomer">
            <SelectParameters>
                <asp:ControlParameter ControlID="gvCustomers" Name="id" PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>
        <br />
        <asp:HyperLink ID="hlRentMedia" runat="server" Visible="False">Rent Media</asp:HyperLink>
    </div>
</asp:Content>
