﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="VideoRentalStore__Assignment2_.Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style2 {
            /*float: left;*/
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <div style="width: 100%; margin: 0 auto; text-align: center">
        <asp:ImageButton ID="ibNewCustomer" runat="server" Height="270px" ImageUrl="~/Images/customer.png" PostBackUrl="~/NewCustomer.aspx" Width="317px" CssClass="auto-style2" />
        <asp:ImageButton ID="ibNewMedia" runat="server" CssClass="auto-style2" Height="270px" ImageUrl="~/Images/media.png" PostBackUrl="~/NewMedia.aspx" Width="342px" />
        <asp:ImageButton ID="ibRent" runat="server" Height="270px" ImageUrl="~/Images/rent.jpg" PostBackUrl="~/RentMedia.aspx" Width="321px" CssClass="auto-style2" />
        <asp:ImageButton ID="ibCustomers" runat="server" Height="270px" ImageUrl="~/Images/customers.jpg" PostBackUrl="~/Customers.aspx" Width="310px" CssClass="auto-style2" />
    </div>
</asp:Content>
