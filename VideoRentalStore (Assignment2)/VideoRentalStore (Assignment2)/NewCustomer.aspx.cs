﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VideoRentalStore__Assignment2_.Models;

namespace VideoRentalStore__Assignment2_
{
    public partial class NewCustomer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;
        }
        public string name { get { return txtFirstName.Text; } }

        public string lastName { get { return txtLastName.Text; } }

        public string address { get { return txtAddress.Text; } }

        public string phone { get { return txtPhone.Text; } }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            //Server.Transfer("~/NewCustomerTarget.aspx");

            Customer newRecord = new Customer
            {
                FirstName = name,
                LastName = lastName,
                Address = address,
                Phone = phone,
            };

            var rep = new DAL_VideoRentalStoreRepository();

            rep.addNewCustomer(newRecord);

            Server.Transfer("~/Home.aspx");
        }
    }
}