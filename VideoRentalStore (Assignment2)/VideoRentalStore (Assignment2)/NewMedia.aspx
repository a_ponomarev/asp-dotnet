﻿<%@ Page Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="NewMedia.aspx.cs" Inherits="VideoRentalStore__Assignment2_.NewMedia" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            text-align: center;
        }
        .auto-style3 {
            width: 134px;
            text-align: left;
        }
        .auto-style4 {
            text-align: left;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    
    <table class="auto-style1">
        <tr>
            <td class="auto-style2" colspan="2"><strong>Add New Media</strong></td>
        </tr>
        <tr>
            <td class="auto-style2" colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style4" colspan="2">
                <asp:ValidationSummary ID="vsErrors" runat="server" Font-Bold="True" Font-Italic="True" ForeColor="Red" />
            </td>
        </tr>
        <tr>
            <td class="auto-style2" colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style3">Title:</td>
            <td class="auto-style4">
                <asp:TextBox ID="txtTitle" runat="server" Width="200px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvTitle" runat="server" ControlToValidate="txtTitle" Display="Dynamic" ErrorMessage="Title is required" Font-Bold="True" ForeColor="Red">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">Type:</td>
            <td class="auto-style4">
                <asp:DropDownList ID="ddlType" runat="server" Width="200px">
                    <asp:ListItem>Please select media type</asp:ListItem>
                    <asp:ListItem>Movie</asp:ListItem>
                    <asp:ListItem>TV Show</asp:ListItem>
                    <asp:ListItem>Documentary</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvType" runat="server" ControlToValidate="ddlType" Display="Dynamic" ErrorMessage="Type is required" Font-Bold="True" ForeColor="Red" InitialValue="Please select media type">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">Production Year:</td>
            <td class="auto-style4">
                <asp:TextBox ID="txtYear" runat="server" Width="200px" TextMode="Number"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvYear" runat="server" ControlToValidate="txtYear" Display="Dynamic" ErrorMessage="Year is required" Font-Bold="True" ForeColor="Red">*</asp:RequiredFieldValidator>
                <asp:RangeValidator ID="rvYear" runat="server" ControlToValidate="txtYear" Display="Dynamic" ErrorMessage="Year must be above 2000" ForeColor="Red" MaximumValue="2018" MinimumValue="2000">*</asp:RangeValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">&nbsp;</td>
            <td class="auto-style4">
                <asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_Click" Text="Add Media" />
            </td>
        </tr>
    </table>
    
</asp:Content>
