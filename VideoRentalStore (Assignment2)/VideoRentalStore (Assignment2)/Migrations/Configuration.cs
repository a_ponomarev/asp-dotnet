namespace VideoRentalStore__Assignment2_.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using VideoRentalStore__Assignment2_.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<VideoRentalStore__Assignment2_.Models.VideoRentalStoreDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(VideoRentalStore__Assignment2_.Models.VideoRentalStoreDBContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            context.RentalRecord.RemoveRange(context.RentalRecord);
            context.Media.RemoveRange(context.Media);
            context.Customer.RemoveRange(context.Customer);

            List<Customer> customers = new List<Customer>();

            customers.Add(new Customer()
            {
                // ID = 1
                FirstName = "Alex",
                LastName = "Ponomarev",
                Address = "Fake str, Montreal, Canada",
                Phone = "5140000000",
                rentalRecords = new List<RentalRecord>
                {
                    new RentalRecord
                    {
                        // ID = 1
                        RentalDate = DateTime.Now,
                        listOfRendtedMedia = new List<Media>
                        {
                            new Media
                            {
                                // ID = 1
                                Title = "Lion King",
                                Type = "Movie",
                                ProdYear = new DateTime(2008, 1, 1)
                            }
                        }
                    }
                }
            });

            customers.Add(new Customer()
            {
                // ID = 2
                FirstName = "Shushan",
                LastName = "Art",
                Address = "Fake str, Laval, Canada",
                Phone = "5140000001",
                rentalRecords = new List<RentalRecord>
                {
                    new RentalRecord
                    {
                        // ID = 1
                        RentalDate = DateTime.Now.AddDays(-7),
                        listOfRendtedMedia = new List<Media>
                        {
                            new Media
                            {
                                // ID = 1
                                Title = "Titanic",
                                Type = "Movie",
                                ProdYear = new DateTime(1998, 1, 1)
                            },

                            new Media
                            {
                                // ID = 2
                                Title = "NatGeo Collection",
                                Type = "Documentary",
                                ProdYear = new DateTime(2007, 1, 1)
                            }
                        }
                    }
                }
            });

            context.Customer.AddRange(customers);
            base.Seed(context);

        }
    }
}

