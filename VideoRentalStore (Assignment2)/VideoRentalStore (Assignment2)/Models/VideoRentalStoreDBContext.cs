﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity; 

namespace VideoRentalStore__Assignment2_.Models
{
    public class VideoRentalStoreDBContext : DbContext
    {
        public DbSet<Customer> Customer { get; set; }
        public DbSet<RentalRecord> RentalRecord { get; set; }
        public DbSet<Media> Media { get; set; }
    }
}