﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VideoRentalStore__Assignment2_.Models;

namespace VideoRentalStore__Assignment2_.Models
{
    public class DAL_VideoRentalStoreRepository
    {

        public void addNewCustomer(Customer c)
        {
            using (VideoRentalStoreDBContext context = new VideoRentalStoreDBContext())
            {
                context.Customer.Add(c);
                context.SaveChanges();
            }
        }

        public void addNewMedia(Media m)
        {
            using (VideoRentalStoreDBContext context = new VideoRentalStoreDBContext())
            {
                context.Media.Add(m);
                context.SaveChanges();
            }
        }

        public List<Customer> getAllCustomers()
        {
            using (VideoRentalStoreDBContext context = new VideoRentalStoreDBContext())
            {
                return (from c in context.Customer
                        select c).ToList();
            }
        }

        public Customer getCustomerByID(int id)
        {
            using (VideoRentalStoreDBContext context = new VideoRentalStoreDBContext())
            {
                return (from c in context.Customer
                        where c.ID == id
                        select c).SingleOrDefault();
            }
        }

        public void updateCustomer(Customer customerFromForm)
        {
            using (VideoRentalStoreDBContext context = new VideoRentalStoreDBContext())
            {
                Customer customerInDb = (from c in context.Customer
                                         where c.ID == customerFromForm.ID
                                         select c).SingleOrDefault();

                if (customerInDb != null)
                {
                    customerInDb.FirstName = customerFromForm.FirstName;
                    customerInDb.LastName = customerFromForm.LastName;
                    customerInDb.Phone = customerFromForm.Phone;
                    customerInDb.Address = customerFromForm.Address;

                    context.SaveChanges();
                }
            }
        }

        public List<Media> getAllMediasByTitle(string title)
        {
            using (VideoRentalStoreDBContext context = new VideoRentalStoreDBContext())
            {
                return (from m in context.Media
                        where m.Title.StartsWith(title) || m.Title.EndsWith(title)
                        select m).ToList();
            }

        }

        public void addRentalRecord(List<int> arr, int customerID)
        {
            using (VideoRentalStoreDBContext context = new VideoRentalStoreDBContext())
            {
                List<Media> list = new List<Media>();

                foreach (int id in arr)
                {
                    Media mediaInDB = (from m in context.Media
                                       where m.ID == id
                                       select m).SingleOrDefault();
                    list.Add(mediaInDB);
                }


                Customer customerFromDB = (from c in context.Customer
                                           where c.ID == customerID
                                           select c).SingleOrDefault();

                customerFromDB.rentalRecords = 
                    new List<RentalRecord> {
                        new RentalRecord {
                            RentalDate = DateTime.Now,
                            listOfRendtedMedia = list,
                        }
                    };

                context.SaveChanges();
            }
        }
    }
}
