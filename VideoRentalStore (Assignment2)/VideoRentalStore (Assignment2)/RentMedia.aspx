﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="RentMedia.aspx.cs" Inherits="VideoRentalStore__Assignment1_.RentMedia" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    .auto-style2 {
        text-align: left;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <h1 class="auto-style1">
    <br />
    Rent Media Form</h1>
<h1 class="auto-style2">
    <asp:TextBox ID="txtTitle" runat="server" placeholder="Enter the title of the media" Height="16px" Width="250px"></asp:TextBox>
    <asp:Button ID="btnSearch" runat="server" Text="Search" />
    </h1>
    <asp:CheckBoxList ID="cblOne" runat="server" DataSourceID="ObjDSGetlAllMediasByTitle" DataTextField="Title" DataValueField="ID">
    </asp:CheckBoxList>
    <asp:ObjectDataSource ID="ObjDSGetlAllMediasByTitle" runat="server" SelectMethod="getAllMediasByTitle" TypeName="VideoRentalStore__Assignment2_.Models.DAL_VideoRentalStoreRepository">
        <SelectParameters>
            <asp:ControlParameter ControlID="txtTitle" Name="title" PropertyName="Text" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <br />
    <asp:Button ID="btnRentNow" runat="server" OnClick="btnRentNow_Click" Text="Rent Now" />
    <br />
    <br />
    <asp:Label ID="lblLog" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
</asp:Content>
