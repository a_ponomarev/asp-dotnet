﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VideoRentalStore__Assignment2_.Models;

namespace VideoRentalStore__Assignment2_
{
    public partial class NewMedia : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;
        }
        public string title { get { return txtTitle.Text; } }

        public string type { get { return ddlType.SelectedValue; } }

        public string year { get { return txtYear.Text; } }


        protected void btnAdd_Click(object sender, EventArgs e)
        {
            // Server.Transfer("~/NewMediaTarget.aspx");

            Media newRecord = new Media
            {
                Title = title,
                Type = type,
                ProdYear = DateTime.Parse(year + "/1/1")
            };

            var rep = new DAL_VideoRentalStoreRepository();

            rep.addNewMedia(newRecord);

            Server.Transfer("~/Home.aspx");
        }
    }
}