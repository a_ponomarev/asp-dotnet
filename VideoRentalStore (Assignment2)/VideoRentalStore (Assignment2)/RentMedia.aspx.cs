﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VideoRentalStore__Assignment2_.Models;

namespace VideoRentalStore__Assignment1_
{
    public partial class RentMedia : System.Web.UI.Page
    {
        int customerID;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["customerID"] != null)
                customerID = int.Parse(Request.QueryString["customerID"]);
            else
                btnRentNow.Enabled = false;
        }

        protected void btnRentNow_Click(object sender, EventArgs e)
        {
            
            List<int> arr = new List<int>();

            for (int i = 0; i < cblOne.Items.Count; i++)
            {
                if (cblOne.Items[i].Selected)
                {
                    arr.Add(int.Parse(cblOne.Items[i].Value));
                }
            }

            DAL_VideoRentalStoreRepository repo = new DAL_VideoRentalStoreRepository();

            repo.addRentalRecord(arr, customerID);

            lblLog.Text = "The rental record for the user has succesfully been created";
        }
    }
}