﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" CodeBehind="NewMediaTarget.aspx.cs" Inherits="VideoRentalStore__Assignment2_.NewMediaTarget" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 150px;
            text-align: left;
        }
        .auto-style3 {
            text-align: left;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <p>
        You have entered the following information:</p>
    <table class="auto-style1">
        <tr>
            <td class="auto-style2">Title:</td>
            <td class="auto-style3">
                <asp:Label ID="lblTitle" runat="server" Font-Bold="True" Font-Italic="True" ForeColor="#FF3300"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">Type:</td>
            <td class="auto-style3">
                <asp:Label ID="lblType" runat="server" Font-Bold="True" Font-Italic="True" ForeColor="#FF3300"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">Production Year:</td>
            <td class="auto-style3">
                <asp:Label ID="lblYear" runat="server" Font-Bold="True" Font-Italic="True" ForeColor="#FF3300"></asp:Label>
            </td>
        </tr>
        </table>
</asp:Content>
