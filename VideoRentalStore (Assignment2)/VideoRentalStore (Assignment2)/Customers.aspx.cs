﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VideoRentalStore__Assignment1_
{
    public partial class Customers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void gvCustomers_SelectedIndexChanged(object sender, EventArgs e)
        {
            hlRentMedia.NavigateUrl = "~/RentMedia.aspx?customerID=" + gvCustomers.SelectedValue;
            hlRentMedia.Visible = true;
        }
    }
}