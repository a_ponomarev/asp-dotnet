﻿using System.Web;
using System.Web.Mvc;

namespace Phono__Assignment_4_
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
