﻿using Phono__Assignment_4_.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phono__Assignment_4_.ViewModels
{
    public class BrandFormViewModel
    {
        public Brand Brand { get; set; }
        public byte ActionSelected { get; set; }
    }
}