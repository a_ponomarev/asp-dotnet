﻿using Phono__Assignment_4_.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Phono__Assignment_4_.ViewModels
{
    public class PhoneFormViewModel
    {
        public Phone Phone { get; set; }
        public IEnumerable<Brand> Brands { get; set; }
        public IEnumerable<PhoneType> PhoneTypes { get; set; }
        public byte ActionSelected { get; set; }
    }
}