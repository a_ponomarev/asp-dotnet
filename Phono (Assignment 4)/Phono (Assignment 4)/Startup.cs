﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Phono__Assignment_4_.Startup))]
namespace Phono__Assignment_4_
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
