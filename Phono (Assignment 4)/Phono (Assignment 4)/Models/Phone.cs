﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Phono__Assignment_4_.Models
{
    public class Phone
    {
        public short Id { get; set; }

        [Required]
        [MaxLength(255)]
        [Display(Name = "Phone Name")]
        public string PhoneName { get; set; }

        public Brand Brand { get; set; }
        [Display(Name = "Brand")]
        public short BrandId { get; set; }

        [Display(Name = "Date Released")]
        public DateTime? DateReleased { get; set; }

        [Range(2.0, 7.0)]
        [Display(Name = "Screen Size")]
        public double? ScreenSize { get; set; }

        public PhoneType PhoneType { get; set; }
        [Display(Name = "Phone Type")]
        public short PhoneTypeId { get; set; }

    }
}