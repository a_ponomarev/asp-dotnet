﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Phono__Assignment_4_.Models
{
    public class PhoneType
    {
        public short Id { get; set; }

        [Required]
        [MaxLength(10)]
        public string Type { get; set; }

        [Required]
        [MaxLength(10)]
        public string OS { get; set; }

        public string FullType
        {
            get
            {
                return String.Format("{0} | {1}", Type, OS);
            }
        }
    }
}