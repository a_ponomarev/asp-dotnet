﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Phono__Assignment_4_.Models
{
    public class Brand
    {
        public short Id { get; set; }

        [Required]
        [MaxLength(255)]
        [Display(Name = "Brand Name")]
        public string BrandName { get; set; }

        [Display(Name = "Country Of Origin")]
        public string CountryOfOrigin { get; set; }
    }
}