namespace Phono__Assignment_4_.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDBTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Phones",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        PhoneName = c.String(),
                        BrandId = c.Short(nullable: false),
                        DateReleased = c.DateTime(),
                        ScreenSize = c.String(),
                        PhoneTypeId = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Brands", t => t.BrandId, cascadeDelete: true)
                .ForeignKey("dbo.PhoneTypes", t => t.PhoneTypeId, cascadeDelete: true)
                .Index(t => t.BrandId)
                .Index(t => t.PhoneTypeId);
            
            CreateTable(
                "dbo.Brands",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        BrandName = c.String(),
                        CountryOfOrigin = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PhoneTypes",
                c => new
                    {
                        Id = c.Short(nullable: false, identity: true),
                        Type = c.String(),
                        OS = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Phones", "PhoneTypeId", "dbo.PhoneTypes");
            DropForeignKey("dbo.Phones", "BrandId", "dbo.Brands");
            DropIndex("dbo.Phones", new[] { "PhoneTypeId" });
            DropIndex("dbo.Phones", new[] { "BrandId" });
            DropTable("dbo.PhoneTypes");
            DropTable("dbo.Brands");
            DropTable("dbo.Phones");
        }
    }
}
