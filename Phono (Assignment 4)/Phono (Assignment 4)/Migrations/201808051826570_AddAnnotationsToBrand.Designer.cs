// <auto-generated />
namespace Phono__Assignment_4_.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class AddAnnotationsToBrand : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddAnnotationsToBrand));
        
        string IMigrationMetadata.Id
        {
            get { return "201808051826570_AddAnnotationsToBrand"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
