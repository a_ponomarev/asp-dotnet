namespace Phono__Assignment_4_.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAnnotationsToPhone : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Phones", "PhoneName", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("dbo.Phones", "ScreenSize", c => c.Double());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Phones", "ScreenSize", c => c.String());
            AlterColumn("dbo.Phones", "PhoneName", c => c.String());
        }
    }
}
