namespace Phono__Assignment_4_.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulatePhoneTypes : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Phonetypes " +
                "VALUES('Smart', 'Android')");

            Sql("INSERT INTO Phonetypes " +
                "VALUES('Smart', 'iOS')");

            Sql("INSERT INTO Phonetypes " +
                "VALUES('Bar', 'N/A')");

            Sql("INSERT INTO Phonetypes " +
                "VALUES('Flip', 'N/A')");
        }
        
        public override void Down()
        {
        }
    }
}
