namespace Phono__Assignment_4_.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAnnotationsToPhoneType : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.PhoneTypes", "Type", c => c.String(nullable: false, maxLength: 10));
            AlterColumn("dbo.PhoneTypes", "OS", c => c.String(nullable: false, maxLength: 10));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PhoneTypes", "OS", c => c.String());
            AlterColumn("dbo.PhoneTypes", "Type", c => c.String());
        }
    }
}
