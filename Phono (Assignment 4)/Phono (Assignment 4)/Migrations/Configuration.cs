namespace Phono__Assignment_4_.Migrations
{
    using Phono__Assignment_4_.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Phono__Assignment_4_.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Phono__Assignment_4_.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            context.Phones.RemoveRange(context.Phones);
            context.Brands.RemoveRange(context.Brands);

            context.Database.ExecuteSqlCommand("DBCC CHECKIDENT ('Brands', RESEED, 1)");

            context.Database.ExecuteSqlCommand("DBCC CHECKIDENT ('Phones', RESEED, 1)");

            context.SaveChanges();

            List<Brand> brands = new List<Brand>
            {
                new Brand {
                    Id = 1,
                    BrandName = "Apple",
                    CountryOfOrigin = "USA"
                },

                new Brand {
                    Id = 2,
                    BrandName = "Samsung",
                    CountryOfOrigin = "Korea"
                },

                new Brand {
                    Id = 3,
                    BrandName = "Nokia",
                    CountryOfOrigin = "Finland"
                },

                new Brand {
                    Id = 4,
                    BrandName = "Some Brand",
                    CountryOfOrigin = "Canada"
                },
            };

            List<Phone> phones = new List<Phone>
            {
                new Phone
                {
                    PhoneName = "Galaxy S8",
                    BrandId = 2,
                    DateReleased = new DateTime(2017,1,1),
                    ScreenSize = 5.5,
                    PhoneTypeId = 1
                },

                new Phone
                {
                    PhoneName = "IPhone X",
                    BrandId = 1,
                    DateReleased = new DateTime(2018, 1, 1),
                    ScreenSize = 6.5,
                    PhoneTypeId = 2
                },

                new Phone
                {
                    PhoneName = "3310",
                    BrandId = 3,
                    DateReleased = new DateTime(2000, 1, 1),
                    ScreenSize = 3.1,
                    PhoneTypeId = 3
                },

                new Phone
                {
                    PhoneName = "Samsung S9",
                    BrandId = 2,
                    DateReleased = new DateTime(2018, 1, 1),
                    ScreenSize = 7.0,
                    PhoneTypeId = 1
                },
            };

            context.Brands.AddRange(brands);
            context.Phones.AddRange(phones);

            base.Seed(context);
        }
    }
}
