﻿using Phono__Assignment_4_.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Phono__Assignment_4_.ViewModels;

namespace Phono__Assignment_4_.Controllers
{
    public class BrandsController : Controller
    {
        //DBContext Object
        private ApplicationDbContext _context;

        public BrandsController()
        {
            _context = new ApplicationDbContext();
        }

        // GET: Brands
        public ActionResult Index()
        {
            var brands = _context.Brands.ToList();

            return View(brands);
        }

        public ActionResult AvailablePhones(int? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            var phonesOfTheBrand = new AvailablePhonesViewModel
            {
                Phones = _context.Phones.Include(p => p.Brand).Where(p => p.Brand.Id == id).ToList(),
                Brand = _context.Brands.FirstOrDefault(b => b.Id == id)
            };

            if (phonesOfTheBrand == null || phonesOfTheBrand.Brand == null)
            {
                return HttpNotFound();
            }

            return View("Phones", phonesOfTheBrand);
        }

        //Details Action
        public ActionResult Details(int? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            var brand = _context.Brands.SingleOrDefault(p => p.Id == id);

            if (brand == null)
            {
                return HttpNotFound();
            }

            return View(brand);
        }

        //Get action to load the form (1)
        public ActionResult New()
        {
            var viewModel = new BrandFormViewModel
            {
                Brand = new Brand
                {
                    Id = 0,
                },
            };

            return View("BrandForm", viewModel);
        }

        //Post action to save data from my form (2)
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Brand brand)
        {
            if (ModelState.IsValid)
            {

                //new brand
                if (brand.Id == 0)
                {
                    _context.Brands.Add(brand);
                }
                else
                {
                    var brandInDB = _context.Brands.Single(b => b.Id == brand.Id);

                    //Manual update
                    brandInDB.BrandName = brand.BrandName;
                    brandInDB.CountryOfOrigin = brand.CountryOfOrigin;
                }

                _context.SaveChanges();
                return RedirectToAction("Index", "Brands");
            }
            else
            {

                var viewModel = new BrandFormViewModel
                {
                    Brand = brand,
                };

                return View("BrandForm", viewModel);
            }
        }

        //Edit brands details
        [Authorize]
        public ActionResult Edit(int id)
        {
            var brandInDb = _context.Brands.SingleOrDefault(b => b.Id == id);

            if (brandInDb == null)
            {
                return HttpNotFound();
            }

            var viewModel = new BrandFormViewModel
            {
                Brand = brandInDb,
                ActionSelected = 1,
            };

            return View("BrandForm", viewModel);
        }

        ////Delete brand
        //[Authorize]
        //public ActionResult Delete(int id)
        //{
        //    var brandInDb = _context.Brands.SingleOrDefault(b => b.Id == id);

        //    if (brandInDb == null)
        //    {
        //        return HttpNotFound();
        //    }

        //    _context.Brands.Remove(brandInDb);
        //    _context.SaveChanges();

        //    return RedirectToAction("Index");
        //}

        //This action will display a confirm msg to confirm the delete operation
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (!id.HasValue)
                return HttpNotFound();

            var brand = _context.Brands.SingleOrDefault(b => b.Id == id);

            if (brand == null)
                return HttpNotFound();

            return View(brand);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var brand = _context.Brands.Find(id);

            _context.Brands.Remove(brand);
            _context.SaveChanges();

            return RedirectToAction("Index", "Brands");
        }
    }
}