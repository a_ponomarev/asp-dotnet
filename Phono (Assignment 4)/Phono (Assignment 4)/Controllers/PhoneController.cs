﻿using Phono__Assignment_4_.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using Phono__Assignment_4_.ViewModels;

namespace Phono__Assignment_4_.Controllers
{
    public class PhoneController : Controller
    {
        //DBContext Object
        private ApplicationDbContext _context;

        public PhoneController()
        {
            _context = new ApplicationDbContext();
        }

        // GET: Phone
        public ActionResult Index()
        {

            var phones = _context.Phones.ToList();

            return View(phones);
        }

        //Details Action
        public ActionResult Details(int? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            var phone = _context.Phones.Include(p => p.Brand).Include(p => p.PhoneType).SingleOrDefault(p => p.Id == id);

            if (phone == null)
            {
                return HttpNotFound();
            }

            return View(phone);
        }

        //Get action to load the form (1)
        public ActionResult New()
        {
            var viewModel = new PhoneFormViewModel
            {
                Phone = new Phone { Id = 0 },
                Brands = _context.Brands.ToArray(),
                PhoneTypes = _context.PhoneTypes.ToArray(),
            };

            return View("PhoneForm", viewModel);
        }

        //Post action to save data from my form (2)
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Phone phone)
        {
            if (ModelState.IsValid)
            {

                //new phone
                if (phone.Id == 0)
                {
                    _context.Phones.Add(phone);
                }
                else
                {
                    var phoneInDB = _context.Phones.Single(p => p.Id == phone.Id);

                    //Manual update
                    phoneInDB.PhoneName = phone.PhoneName;
                    phoneInDB.DateReleased = phone.DateReleased;
                    phoneInDB.BrandId = phone.BrandId;
                    phoneInDB.ScreenSize = phone.ScreenSize;
                    phoneInDB.PhoneTypeId = phone.PhoneTypeId;
                }

                _context.SaveChanges();
                return RedirectToAction("Index", "Phone");
            }
            else
            {

                var viewModel = new PhoneFormViewModel
                {
                    Phone = phone,
                    Brands = _context.Brands.ToArray(),
                    PhoneTypes = _context.PhoneTypes.ToArray(),
                };

                return View("PhoneForm", viewModel);
            }
        }

        //Edit phone details
        [Authorize]
        public ActionResult Edit(int id)
        {
            var phoneInDb = _context.Phones.SingleOrDefault(p => p.Id == id);

            if (phoneInDb == null)
            {
                return HttpNotFound();
            }

            var viewModel = new PhoneFormViewModel()
            {
                Phone = phoneInDb,
                Brands = _context.Brands.ToArray(),
                PhoneTypes = _context.PhoneTypes.ToArray(),
                ActionSelected = 1
            };

            return View("PhoneForm", viewModel);
        }

        ////Delete phone
        //[Authorize]
        //public ActionResult Delete(int id)
        //{
        //    var phoneInDb = _context.Phones.SingleOrDefault(p => p.Id == id);

        //    if (phoneInDb == null)
        //    {
        //        return HttpNotFound();
        //    }

        //    _context.Phones.Remove(phoneInDb);
        //    _context.SaveChanges();

        //    return RedirectToAction("Index");
        //}

        //This action will display a confirm msg to confirm the delete operation
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (!id.HasValue)
                return HttpNotFound();

            var phone = _context.Phones.Include(b => b.Brand).SingleOrDefault(p => p.Id == id);

            if (phone == null)
                return HttpNotFound();

            return View(phone);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var phone = _context.Phones.Find(id);

            _context.Phones.Remove(phone);
            _context.SaveChanges();

            return RedirectToAction("Index", "Phone");
        }
    }
}