﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OrdersSearch
{
    public partial class Default : System.Web.UI.Page
    {
        private string connString = ConfigurationManager.ConnectionStrings["NothwindConnString_ADO"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnQuery_Click(object sender, EventArgs e)
        {
            // 1 - SQL Connection 

            SqlConnection con = new SqlConnection(connString);

            try
            {
                int flag = 0;
                string query = "select * from Orders where OrderID=" + txtOrderID.Text;
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();

                // getting data 

                SqlDataReader reader = cmd.ExecuteReader();

                // binding
                dvOrderDetails.DataSource = reader;
                dvOrderDetails.DataBind();
                con.Close();

                lbDetails.Items.Clear();

                switch (ddlSelectOption.SelectedValue)
                {
                    case "0":
                        query = "select * from Customers C inner join Orders O on C.CustomerID = O.CustomerID inner join [dbo].[Order Details] OD on OD.OrderID = O.OrderID where O.OrderID=" + txtOrderID.Text;
                        break;
                    case "1":
                        query = "select S.CompanyName from Shippers S inner join Orders O on S.ShipperID = O.ShipVia where O.OrderID=" + txtOrderID.Text;
                        flag = 1;
                        break;

                }

                cmd = new SqlCommand(query, con);
                con.Open();

                reader = cmd.ExecuteReader();


                while (reader.Read())
                {
                    string entry = null;

                    if (flag == 0)
                    {
                        entry = "Customer ID: " + reader["CustomerID"] + "; Company Name " + reader["CompanyName"] + "; City: " + reader["City"] + "; Country " + reader["Country"];
                    }
                    else
                    {
                        entry = "Company Name: " + reader["CompanyName"];
                    }

                    lbDetails.Items.Add(entry);
                }
            }
            catch (Exception)
            {

            }
            finally
            {
                con.Close();
            }
        }


    }
}