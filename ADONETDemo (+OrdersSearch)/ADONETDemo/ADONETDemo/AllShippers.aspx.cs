﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ADONETDemo
{
    public partial class AllShippers : System.Web.UI.Page
    {
        private string connString = ConfigurationManager.ConnectionStrings["NothwindConnString_ADO"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                return;
            }

            // 1 - SQL connection
            SqlConnection con = null;

            try
            {
                con = new SqlConnection(connString);
                string sqlQuery = "select * from Shippers";
                SqlCommand cmd = new SqlCommand(sqlQuery, con);

                con.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                gvSuppliers.DataSource = reader;
                gvSuppliers.DataBind();


            }
            catch (Exception)
            {

            }
            finally
            {
                con.Close();
            }
        }
    }
}