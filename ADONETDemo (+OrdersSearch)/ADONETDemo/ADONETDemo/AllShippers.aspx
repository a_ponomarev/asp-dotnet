﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AllShippers.aspx.cs" Inherits="ADONETDemo.AllShippers" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            text-align: center;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div class="auto-style1">
                <h1>Shipper&#39;s List<br />
                </h1>
            </div>
            <div>
                <asp:GridView ID="gvSuppliers" runat="server" Height="126px" Width="908px">
                </asp:GridView>
            </div>
        </div>
    </form>
</body>
</html>
