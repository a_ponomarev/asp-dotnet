﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewShipper.aspx.cs" Inherits="ADONETDemo.NewShipper" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            text-align: center;
        }
        .auto-style3 {
            margin-left: 0px;
        }
        .auto-style4 {
            width: 151px;
        }
        .auto-style5 {
            width: 229px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div class="auto-style2">
                <h1>New Shipper Form<br />
                </h1>
            </div>
            <table class="auto-style1">
                <tr>
                    <td class="auto-style4">Company Name</td>
                    <td class="auto-style5">
                        <asp:TextBox ID="txtCompanyName" runat="server" CssClass="auto-style3" Width="200px"></asp:TextBox>
                    </td>
                    <td><strong>
                        <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtCompanyName" ErrorMessage="Company Name is required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </strong></td>
                </tr>
                <tr>
                    <td class="auto-style4">Phone Number</td>
                    <td class="auto-style5">
                        <asp:TextBox ID="txtPhone" runat="server" Width="200px"></asp:TextBox>
                    </td>
                    <td><strong>
                        <asp:RequiredFieldValidator ID="rfvPhone" runat="server" ControlToValidate="txtPhone" ErrorMessage="Phone number is required" ForeColor="Red"></asp:RequiredFieldValidator>
                        </strong></td>
                </tr>
                <tr>
                    <td class="auto-style4">&nbsp;</td>
                    <td class="auto-style5">
                        <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Save" />
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style4">&nbsp;</td>
                    <td class="auto-style5">
                        <asp:Label ID="lblLog" runat="server"></asp:Label>
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
