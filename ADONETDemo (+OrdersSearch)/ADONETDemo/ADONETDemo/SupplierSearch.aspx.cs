﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace ADONETDemo
{
    public partial class SupplierSearch : System.Web.UI.Page
    {
        private string connString = ConfigurationManager.ConnectionStrings["NothwindConnString_ADO"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                return;
            }

            // 1 - SQL connection
            SqlConnection con = null;

            try
            {
                con = new SqlConnection(connString);
                string sqlQuery = "select * from Suppliers";
                SqlCommand cmd = new SqlCommand(sqlQuery, con);

                con.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                ListItem temp = new ListItem("Please select a supplier", "");
                ddlSupplierName.Items.Add(temp);

                while (reader.Read())
                {
                    ListItem supplier = new ListItem();

                    supplier.Text = reader["CompanyName"].ToString();
                    supplier.Value = reader["SupplierID"].ToString();

                    ddlSupplierName.Items.Add(supplier);

                }

                // lblResult.Text = "Connection OK";

            }
            catch (Exception)
            {
                lblResult.Text = "Error in connection";

            }
            finally
            {
                con.Close();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            lbResult.Items.Clear();

            // 1 - SQL Connection 

            SqlConnection con = new SqlConnection(connString);

            try
            {
                string query = "select * from Suppliers where SupplierID=" + txtID.Text;
                SqlCommand cmd = new SqlCommand(query, con);
                con.Open();

                // getting data 

                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    lbResult.Items.Add("ID: " + reader["SupplierID"]);
                    lbResult.Items.Add("Name: " + reader["CompanyName"]);
                    lbResult.Items.Add("Address: " + reader["Address"]);
                    lbResult.Items.Add("Phone: " + reader["Phone"]);
                }
                else
                {
                    lbResult.Items.Add("No record found");
                }
            }
            catch (Exception)
            {

            }
        }

        protected void ddlSupplierName_SelectedIndexChanged(object sender, EventArgs e)
        {
            // 1 - SQL Connection 

            SqlConnection con = new SqlConnection(connString);

            try
            {
                if (ddlSupplierName.SelectedIndex != 0)
                {
                    string query = "select * from Suppliers where SupplierID=" + ddlSupplierName.SelectedValue;
                    SqlCommand cmd = new SqlCommand(query, con);
                    con.Open();

                    // getting data 

                    SqlDataReader reader = cmd.ExecuteReader();

                    // binding
                    dvSupplier.DataSource = reader;
                    dvSupplier.DataBind();
                }
            }
            catch (Exception)
            {

            } finally
            {
                con.Close();
            }

        }
    }
}