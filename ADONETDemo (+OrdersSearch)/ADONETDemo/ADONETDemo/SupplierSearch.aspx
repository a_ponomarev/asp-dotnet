﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SupplierSearch.aspx.cs" Inherits="ADONETDemo.SupplierSearch" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 593px;
        }
        .auto-style3 {
            width: 593px;
            text-align: center;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table class="auto-style1">
                <tr>
                    <td>Supplier ID:</td>
                    <td class="auto-style3">
                        <asp:TextBox ID="txtID" runat="server" Height="22px" Width="494px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Button ID="btnSearch" runat="server" Height="23px" OnClick="btnSearch_Click" Text="Search" Width="110px" />
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="auto-style3">
                        <asp:ListBox ID="lbResult" runat="server" Height="92px" Width="504px"></asp:ListBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="auto-style2">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Supplier Name:</td>
                    <td class="auto-style3">
                        <asp:DropDownList ID="ddlSupplierName" runat="server" AutoPostBack="True" Height="16px" OnSelectedIndexChanged="ddlSupplierName_SelectedIndexChanged" Width="496px">
                        </asp:DropDownList>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="auto-style2">
                        <asp:DetailsView ID="dvSupplier" runat="server" Height="30px" Width="575px">
                        </asp:DetailsView>
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </table>
            <br />
            <br />
            <asp:Label ID="lblResult" runat="server"></asp:Label>
        </div>
    </form>
</body>
</html>
