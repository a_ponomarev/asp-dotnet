﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ADONETDemo
{
    public partial class NewShipper : System.Web.UI.Page
    {
        private string connString = ConfigurationManager.ConnectionStrings["NothwindConnString_ADO"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {
            UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            // 1 - SQL connection
            SqlConnection con = null;

            try
            {
                con = new SqlConnection(connString);
                string sqlQuery = "insert into Shippers values('" + txtCompanyName.Text + "','" + txtPhone.Text + "')";
                SqlCommand cmd = new SqlCommand(sqlQuery, con);

                con.Open();

                cmd.ExecuteNonQuery();
                lblLog.Text = "Shipper added succesfully!";

                Response.Redirect("~/AllShippers.aspx");
            }
            catch (Exception)
            {

            }
            finally
            {
                con.Close();
            }
        }
    }
}