﻿using SurveyOnline.Models;
using SurveyOnline.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace SurveyOnline.Controllers
{
    public class SurveyController : Controller
    {
        //DBContext Object
        private ApplicationDbContext _context;

        public SurveyController()
        {
            _context = new ApplicationDbContext();
        }

        [Authorize]
        public ActionResult Index()
        {
            var surveys = _context.Surveys.ToList();

            return View(surveys);
        }

        [Authorize]
        public ActionResult New()
        {
            var viewModel = new SurveyFormViewModel
            {
                Survey = new Survey(),

                 Categories = _context.Categories.ToArray(),
            };

            return View("SurveyForm", viewModel);
        }

        //Post action to save data from my form (2)
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Survey survey)
        {
            if (!ModelState.IsValid)
            {
                //Return the same form back to the user
                var viewModel = new SurveyFormViewModel
                {
                    Survey = survey,
                    Categories = _context.Categories.ToArray(),
                };

                return View("SurveyForm", viewModel);
            }

            //new media
            if (survey.Id == 0)
            {
                _context.Surveys.Add(survey);
            }
            else
            {
                var surveyInDB = _context.Surveys.Single(s => s.Id == survey.Id);

                //Manual update
                surveyInDB.Title = survey.Title;
                surveyInDB.SurveyQuestion = survey.SurveyQuestion;
                surveyInDB.CategoryId = survey.CategoryId;
                surveyInDB.DealineDate = survey.DealineDate;
                surveyInDB.MinimumResponses = survey.MinimumResponses;
            }

            _context.SaveChanges();
            return RedirectToAction("Index", "Survey");
        }

        public ActionResult Details(short? id)
        {
            if (!id.HasValue)
            {
                return HttpNotFound();
            }

            var surveys = _context.Surveys.Include(c => c.Category).SingleOrDefault(m => m.Id == id);

            if (surveys == null)
            {
                return HttpNotFound();
            }

            return View(surveys);
        }

        [Authorize]
        public ActionResult Edit(short id)
        {
            var surveyInDb = _context.Surveys.SingleOrDefault(s => s.Id == id);

            if (surveyInDb == null)
            {
                return HttpNotFound();
            }

            var viewModel = new SurveyFormViewModel()
            {
                Survey = surveyInDb,
                Categories = _context.Categories.ToList(),
            };

            return View("SurveyForm", viewModel);
        }

        [Authorize]
        public ActionResult Delete(short? id)
        {
            if (!id.HasValue)
                return HttpNotFound();

            var survey = _context.Surveys.Include(c => c.Category).SingleOrDefault(s => s.Id == id);

            if (survey == null)
                return HttpNotFound();

            return View(survey);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(short id)
        {
            var survey = _context.Surveys.Find(id);

            _context.Surveys.Remove(survey);
            _context.SaveChanges();

            return RedirectToAction("Index", "Survey");
        }
    }
}