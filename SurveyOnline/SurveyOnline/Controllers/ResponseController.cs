﻿using SurveyOnline.Models;
using SurveyOnline.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace SurveyOnline.Controllers
{
    public class ResponseController : Controller
    {
        //DBContext Object
        private ApplicationDbContext _context;

        public ResponseController()
        {
            _context = new ApplicationDbContext();
        }

        public ActionResult Index(string searchString)
        {
            var surveys = _context.Surveys.Include(c => c.Category);

            if (!string.IsNullOrEmpty(searchString))
            {
                surveys = surveys.Where(m => m.Title.Contains(searchString));
            }

            return View(surveys);
        }

        public ActionResult Answer(short? id)
        {
            if (!id.HasValue)
                return HttpNotFound();

            var viewModel = new AnswerFormViewModel
            {
                Survey = _context.Surveys.SingleOrDefault(s => s.Id == id),

                Response = new Response() {
                    SurveyId = (short)id
                },
            };


            if (viewModel.Survey == null)
                return HttpNotFound();

            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Response response)
        {
            if (!ModelState.IsValid)
            {
                //Return the same form back to the user
                var viewModel = new AnswerFormViewModel
                {
                    Survey = _context.Surveys.SingleOrDefault(s => s.Id == response.SurveyId),

                    Response = response,
                };

                return View("Answer", viewModel);
            }

            //new media
            if (response.Id == 0)
            {
                _context.Responses.Add(response);
            }
            

            _context.SaveChanges();
            return RedirectToAction("AnswerConfirmation", response);
        }

        public ActionResult AnswerConfirmation()
        {
         

            return View();
        }
    }
}