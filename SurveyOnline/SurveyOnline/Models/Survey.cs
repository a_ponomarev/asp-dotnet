﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SurveyOnline.Models
{
    public class Survey
    {
        public short Id { get; set; }

        [Required]
        [MaxLength(255)]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Survey Question")]
        public string SurveyQuestion { get; set; }

        public Category Category { get; set; }

        [Required]
        [Display(Name = "Category")]
        public short CategoryId { get; set; }

        [Display(Name = "Deadline Date")]
        public DateTime? DealineDate { get; set; }

        [Range(3, 100)]
        [Display(Name = "Minimum Responses")]
        public short MinimumResponses { get; set; }

    }
}