﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SurveyOnline.Models
{
    public class Category
    {
        public short Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Topic { get; set; }
    }
}