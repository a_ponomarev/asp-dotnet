﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SurveyOnline.Models
{
    public class Response
    {
        public short Id { get; set; }

        [Required]
        [MaxLength(255)]
        [Display(Name = "Responder Name")]
        public string ResponderName { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        [Compare("Email", ErrorMessage = "The email and confirmation do not match.")]
        [Display(Name = "Confirm Email")]
        public string ConfirmEmail { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Phone format: 514-000-0000")]
        public string Phone { get; set; }

        public Survey Survey { get; set; }
        public short SurveyId { get; set; }

        [Required]
        [Display(Name = "Survey Answer")]
        public string SurveyAnswer { get; set; }
    }
}