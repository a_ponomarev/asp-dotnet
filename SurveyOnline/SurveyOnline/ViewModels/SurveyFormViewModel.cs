﻿using SurveyOnline.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SurveyOnline.ViewModels
{
    public class SurveyFormViewModel
    {
        public Survey Survey { get; set; }
        public IEnumerable<Category> Categories { get; set; }
    }
}