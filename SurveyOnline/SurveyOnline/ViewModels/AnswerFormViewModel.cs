﻿using SurveyOnline.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SurveyOnline.ViewModels
{
    public class AnswerFormViewModel
    {
        public Survey Survey { get; set; }
        public Response Response { get; set; }
    }
}