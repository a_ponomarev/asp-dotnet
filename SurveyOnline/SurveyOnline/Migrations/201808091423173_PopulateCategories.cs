namespace SurveyOnline.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class PopulateCategories : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Categories " +
                " Values('IT')");

            Sql("INSERT INTO Categories " +
                " Values('Science')");

            Sql("INSERT INTO Categories " +
                " Values('Social')");

            Sql("INSERT INTO Categories " +
                " Values('Other')");
        }

        public override void Down()
        {
        }
    }
}
