namespace SurveyOnline.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCompareEmailField : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Responses", "ConfirmEmail", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Responses", "ConfirmEmail");
        }
    }
}
