// <auto-generated />
namespace SurveyOnline.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class AddCompareEmailField : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddCompareEmailField));
        
        string IMigrationMetadata.Id
        {
            get { return "201808091616392_AddCompareEmailField"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
