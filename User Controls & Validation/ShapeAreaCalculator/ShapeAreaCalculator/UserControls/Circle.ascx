﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Circle.ascx.cs" Inherits="ShapeAreaCalculator.UserControls.Circle" %>
<style type="text/css">
    .auto-style2 {
        width: 100%;
    }
    .auto-style3 {
        text-align: center;
        
    }
    .auto-style6 {
        width: 50%;
        text-align: right;
        padding-right: 150px;
    }
    .auto-style7 {
        text-align: left;
    }
    .auto-style8 {
        width: 141px;
        height: 159px;
    }
</style>

<div class="auto-style3">

<table class="auto-style2">
    <tr>
        <td class="auto-style3" colspan="2">
            <h2>Circle Area Calculator</h2>
        </td>
    </tr>
    <tr>
        <td class="auto-style3" colspan="2">
            <img alt="" class="auto-style8" src="../Images/circle.jpg" /></td>
    </tr>
    <tr>
        <td class="auto-style6">Radius</td>
        <td class="auto-style7">
            <asp:TextBox ID="txtRadius" runat="server" Width="199px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvRadius" runat="server" ControlToValidate="txtRadius" ErrorMessage="Radius is required"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="auto-style3" colspan="2">
            <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
        </td>
    </tr>
</table>

    <br />
    <br />
    <br />
    <asp:Label ID="lblResults" runat="server" Font-Bold="True" Font-Italic="True"></asp:Label>
</div>


