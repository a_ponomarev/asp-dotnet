﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Square.ascx.cs" Inherits="ShapeAreaCalculator.UserControls.Square" %>
<style type="text/css">
    .auto-style2 {
        width: 100%;
    }
    .auto-style3 {
        text-align: center;
        
    }
    .auto-style4 {
        width: 124px;
        height: 150px;
    }
    .auto-style6 {
        width: 50%;
        text-align: right;
        padding-right: 150px;
    }
    .auto-style7 {
        text-align: left;
    }
</style>

<div class="auto-style3">

<table class="auto-style2">
    <tr>
        <td class="auto-style3" colspan="2">
            <h2>Square Area Calculator</h2>
        </td>
    </tr>
    <tr>
        <td class="auto-style3" colspan="2">
            <img class="auto-style4" src="../Images/square.jpg" /></td>
    </tr>
    <tr>
        <td class="auto-style6">Width</td>
        <td class="auto-style7">
            <asp:TextBox ID="txtWidth" runat="server" Width="199px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvWidth" runat="server" ControlToValidate="txtWidth" ErrorMessage="Width is required"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="auto-style3" colspan="2">
            <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
        </td>
    </tr>
</table>

    <br />
    <br />
    <br />
    <asp:Label ID="lblResults" runat="server" Font-Bold="True" Font-Italic="True"></asp:Label>
</div>


