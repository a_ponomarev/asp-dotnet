﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Triangle.ascx.cs" Inherits="ShapeAreaCalculator.UserControls.Triangle" %>
<style type="text/css">
    .auto-style2 {
        width: 100%;
    }
    .auto-style3 {
        text-align: center;
        
    }
    .auto-style6 {
        width: 50%;
        text-align: right;
        padding-right: 150px;
    }
    .auto-style7 {
        text-align: left;
    }
    .auto-style8 {
        width: 113px;
        height: 155px;
    }
</style>

<div class="auto-style3">

<table class="auto-style2">
    <tr>
        <td class="auto-style3" colspan="2">
            <h2>Triangle Area Calculator</h2>
        </td>
    </tr>
    <tr>
        <td class="auto-style3" colspan="2">
            <img class="auto-style8" src="../Images/triangle.jpg" /></td>
    </tr>
    <tr>
        <td class="auto-style6">Area of the base</td>
        <td class="auto-style7">
            <asp:TextBox ID="txtBase" runat="server" Width="199px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvBase" runat="server" ControlToValidate="txtBase" ErrorMessage="Base is required"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="auto-style6">Height</td>
        <td class="auto-style7">
            <asp:TextBox ID="txtHeight" runat="server" Width="199px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvHeight" runat="server" ControlToValidate="txtHeight" ErrorMessage="Height is required"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="auto-style3" colspan="2">
            <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
        </td>
    </tr>
</table>

    <br />
    <br />
    <br />
    <asp:Label ID="lblResults" runat="server" Font-Bold="True" Font-Italic="True"></asp:Label>
</div>


