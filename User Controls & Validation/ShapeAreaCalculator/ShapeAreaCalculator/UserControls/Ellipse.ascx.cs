﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ShapeAreaCalculator.UserControls
{


    public partial class Ellipse : System.Web.UI.UserControl
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            lblResults.Text = (Math.PI * double.Parse(txtAxisA.Text) * double.Parse(txtAxisB.Text)).ToString() + " m";
        }
    }
}