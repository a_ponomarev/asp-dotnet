﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Ellipse.ascx.cs" Inherits="ShapeAreaCalculator.UserControls.Ellipse" %>
<style type="text/css">
    .auto-style2 {
        width: 100%;
    }
    .auto-style3 {
        text-align: center;
        
    }
    .auto-style6 {
        width: 50%;
        text-align: right;
        padding-right: 150px;
    }
    .auto-style7 {
        text-align: left;
    }
    .auto-style9 {
        width: 143px;
        height: 123px;
    }
</style>

<div class="auto-style3">

<table class="auto-style2">
    <tr>
        <td class="auto-style3" colspan="2">
            <h2>Ellipse Area Calculator</h2>
        </td>
    </tr>
    <tr>
        <td class="auto-style3" colspan="2">
            <img alt="" class="auto-style9" src="../Images/ellipse.jpg" /></td>
    </tr>
    <tr>
        <td class="auto-style6">Long axis</td>
        <td class="auto-style7">
            <asp:TextBox ID="txtAxisA" runat="server" Width="199px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvAxisA" runat="server" ControlToValidate="txtAxisA" ErrorMessage="Axis A is required"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="auto-style6">Short axis</td>
        <td class="auto-style7">
            <asp:TextBox ID="txtAxisB" runat="server" Width="199px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvAxisB" runat="server" ControlToValidate="txtAxisB" ErrorMessage="Axis B is required"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="auto-style3" colspan="2">
            <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
        </td>
    </tr>
</table>

    <br />
    <br />
    <br />
    <asp:Label ID="lblResults" runat="server" Font-Bold="True" Font-Italic="True"></asp:Label>
</div>


