﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ShapeAreaCalculator.UserControls
{


    public partial class Triangle : System.Web.UI.UserControl
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            lblResults.Text = (double.Parse(txtBase.Text) / 2 * double.Parse(txtHeight.Text)).ToString() + " m";
        }
    }
}