﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Trapezoid.ascx.cs" Inherits="ShapeAreaCalculator.UserControls.Trapezoid" %>
<style type="text/css">
    .auto-style2 {
        width: 100%;
    }
    .auto-style3 {
        text-align: center;
        
    }
    .auto-style6 {
        width: 50%;
        text-align: right;
        padding-right: 150px;
    }
    .auto-style7 {
        text-align: left;
    }
    .auto-style10 {
        width: 136px;
        height: 147px;
    }
</style>

<div class="auto-style3">

<table class="auto-style2">
    <tr>
        <td class="auto-style3" colspan="2">
            <h2>Trapezoid Area Calculator</h2>
        </td>
    </tr>
    <tr>
        <td class="auto-style3" colspan="2">
            <img alt="" class="auto-style10" src="../Images/trapezoid.jpg" /></td>
    </tr>
    <tr>
        <td class="auto-style6">Length of the top</td>
        <td class="auto-style7">
            <asp:TextBox ID="txtTop" runat="server" Width="199px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvTop" runat="server" ControlToValidate="txtTop" ErrorMessage="Length of the top is required"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="auto-style6">Length of the base</td>
        <td class="auto-style7">
            <asp:TextBox ID="txtBase" runat="server" Width="199px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvBase" runat="server" ControlToValidate="txtBase" ErrorMessage="Length of the base is required"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="auto-style6">Height</td>
        <td class="auto-style7">
            <asp:TextBox ID="txtHeight" runat="server" Width="199px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvHeight" runat="server" ControlToValidate="txtHeight" ErrorMessage="Height is required"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="auto-style3" colspan="2">
            <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
        </td>
    </tr>
</table>

    <br />
    <br />
    <br />
    <asp:Label ID="lblResults" runat="server" Font-Bold="True" Font-Italic="True"></asp:Label>
</div>


