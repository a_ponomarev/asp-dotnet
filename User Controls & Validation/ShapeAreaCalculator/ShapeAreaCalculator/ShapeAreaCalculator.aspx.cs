﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ShapeAreaCalculator
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
             
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;

            ViewState["control"] = rblOne.SelectedValue.ToString() + ".ascx";

            if (Page.IsPostBack)
            {
                if (ViewState["control"] == null)
                    return;

            } 

            phOne.Controls.Add(Page.LoadControl("~/UserControls/" + (string)ViewState["control"]));

        }

        protected void rblOne_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}