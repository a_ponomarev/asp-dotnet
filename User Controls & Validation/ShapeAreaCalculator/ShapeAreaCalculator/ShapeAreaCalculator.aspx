﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShapeAreaCalculator.aspx.cs" Inherits="ShapeAreaCalculator.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            text-align: center;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        

        <table class="auto-style1">
        <tr>
            <td>
                <h1 class="auto-style2">Shape Area Calculator</h1>
            </td>
        </tr>
        <tr>
            <td>
                <asp:RadioButtonList ID="rblOne" runat="server" AutoPostBack="True" Height="37px" Width="127px" OnSelectedIndexChanged="rblOne_SelectedIndexChanged">
                    <asp:ListItem Selected="True">Square</asp:ListItem>
                    <asp:ListItem>Rectangle</asp:ListItem>
                    <asp:ListItem>Triangle</asp:ListItem>
                    <asp:ListItem>Circle</asp:ListItem>
                    <asp:ListItem>Ellipse</asp:ListItem>
                    <asp:ListItem>Trapezoid</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><div>
            <asp:PlaceHolder ID="phOne" runat="server"></asp:PlaceHolder>
        </div></td>
        </tr>
    </table>
    </form>
    
</body>
</html>
