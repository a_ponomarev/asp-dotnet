﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace InvestmentCalculator
{
    public partial class InvestmentCalculator : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;

            Page.SetFocus(txtName);

            if (!Page.IsPostBack) {
                // populating drop down list
                for (int i = 50; i <= 500; i += 50)
                {
                    ddlInvestment.Items.Add(i.ToString());
                }
            }
            

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            // investment
            int monthlyInvestment = int.Parse(ddlInvestment.SelectedValue);

            // interest
            decimal interest = decimal.Parse(txtInterest.Text);

            // years
            int years = int.Parse(txtYears.Text);

            decimal calculateInvestment = CalculateValue(monthlyInvestment, interest, years);

            txtResults.Text = "With a monthly investement of " + monthlyInvestment + "$\n" + "Interest Rate " + interest + "%\n" +
                "For " + years + " years \n" + "The calculated investment will be " + calculateInvestment.ToString("c");
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            // name
            txtName.Text = string.Empty;

            // investment
            ddlInvestment.SelectedIndex = 0;

            // interest
            txtInterest.Text = string.Empty;

            // years
            txtYears.Text = string.Empty;

            // results
            txtResults.Text = "";
        }

        protected decimal CalculateValue(int monthlyInvestment, decimal yearlyInterestRate, int years)
        {
            int months = years * 12;
            decimal monthlyInterestRate = yearlyInterestRate / 12 / 100;
            decimal futureValue = 0;
            for (int i = 0; i < months; i++)
            {
                futureValue = (futureValue + monthlyInvestment) * (1 + monthlyInterestRate);
            }
            return futureValue;
        }
    }
}