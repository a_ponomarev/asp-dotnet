﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MySite.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="InvestmentCalculator.Home" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    
    <div>
            <table class="auto-style5">
            <tr>
                <td colspan="2" style="text-align: center">
                    <a href="Home.aspx?print=true">Printable Version</a>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center">
                    <h1>Investment Calculator</h1>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">Name</td>
                <td>
                    <asp:TextBox ID="txtName" runat="server" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" Display="Dynamic" ErrorMessage="The name field is required" ForeColor="Red">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">Monthly Investment</td>
                <td>
                    <asp:DropDownList ID="ddlInvestment" runat="server" Width="200px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">Annual Interest Rate</td>
                <td>
                    <asp:TextBox ID="txtInterest" runat="server" Width="200px"></asp:TextBox>
                    <asp:RangeValidator ID="rvInterest" runat="server" ControlToValidate="txtInterest" Display="Dynamic" ErrorMessage="Interest has to be between 1.0 and 20.0" ForeColor="Red" MaximumValue="20.0" MinimumValue="1.0" Type="Double">*</asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvInterest" runat="server" ControlToValidate="txtInterest" Display="Dynamic" ErrorMessage="The interest field is required" ForeColor="Red">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">Number of Years</td>
                <td>
                    <asp:TextBox ID="txtYears" runat="server" Width="200px"></asp:TextBox>
                    <asp:RangeValidator ID="rvYears" runat="server" ControlToValidate="txtYears" Display="Dynamic" ErrorMessage="Number of years between 1 and 45" ForeColor="Red" MaximumValue="45" MinimumValue="1" Type="Integer">*</asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvYears" runat="server" ControlToValidate="txtYears" Display="Dynamic" ErrorMessage="The years field is required" ForeColor="Red">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style4">
                    <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="Submit" Width="200px" />
                </td>
                <td class="auto-style2">
                    <asp:Button ID="btnClear" runat="server" OnClick="btnClear_Click" style="height: 26px" Text="Clear" Width="200px" />
                </td>
            </tr>
            <tr>
                <td class="auto-style6" colspan="2">
                    <asp:ValidationSummary ID="vs" runat="server" ForeColor="Red" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:TextBox ID="txtResults" runat="server" ForeColor="Red" Height="103px" ReadOnly="True" TextMode="MultiLine" Width="415px"></asp:TextBox>
                </td>
            </tr>
        </table>
        </div>
</asp:Content>
