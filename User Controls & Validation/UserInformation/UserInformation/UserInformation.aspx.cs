﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace UserInformation
{
    public partial class UserInformation : System.Web.UI.Page
    {
        bool flag = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;

        }

        protected void ddlProvince_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlCity.Items.Clear();
            String selection = ddlProvince.SelectedItem.Text;

            ddlCity.Items.Add("Please select City");

            switch (selection)
            {
                case "ON":

                    ddlCity.Items.Add("Toronto");
                    break;
                case "QC":
                    ddlCity.Items.Add("Montreal");
                    break;
                case "BC":
                    ddlCity.Items.Add("Vancouver");
                    break;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            Page.Validate();
            if (Page.IsValid)
            {
                String currentResult = txtName.Text.ToString() + " | " + txtEmail.Text.ToString() + " | " + txtDate.Text.ToString() + " | " + ddlProvince.SelectedValue.ToString() + " | " + ddlCity.SelectedValue.ToString();

                lblResults.Text = "Entry sent succesfully. Number of records " + updateCounter();

                object value = Session["flag"];
                if (value != null)
                {
                    flag = (bool)value;
                }
                else
                {
                    flag = false; 
                }

                if (flag) {
                    int index = lbOne.SelectedIndex;
                    lbOne.Items.RemoveAt(index);
                    lbOne.Items.Insert(index, currentResult);
                    clearFields();
                    Session["flag"] = !flag;
                    return;
                }
                
                lbOne.Items.Add(currentResult);
                clearFields();

            }
        }

        private int updateCounter()
        {
            int counter = int.Parse(recordCounter.Value.ToString());
            counter++;
            recordCounter.Value = counter.ToString();
            return counter;
        }

        private void clearFields()
        {
            txtName.Text = string.Empty;
            txtDate.Text = string.Empty;
            txtEmail.Text = string.Empty;
            ddlCity.ClearSelection();
            ddlProvince.ClearSelection();
        }

        protected void lbOne_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedItem = lbOne.SelectedValue;

            lblResults.Text = selectedItem;
            String[] modifyingItem = selectedItem.Split('|');

            for (int i = 0; i < modifyingItem.Length; i++)
            {
                modifyingItem[i] = modifyingItem[i].Trim();
            }

            txtName.Text = modifyingItem[0];
            txtEmail.Text = modifyingItem[1];
            txtDate.Text = modifyingItem[2];
            ddlProvince.SelectedValue = modifyingItem[3];
            ddlCity.SelectedValue = modifyingItem[4];

            flag = !flag;

            Session["flag"] = flag;

        }
    }
}