﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserInformation.aspx.cs" Inherits="UserInformation.UserInformation" %>

<%@ Register src="UserControls/UIHeader.ascx" tagname="UIHeader" tagprefix="uc1" %>
<%@ Register src="UserControls/UIFooter.ascx" tagname="UIFooter" tagprefix="uc2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }

        .auto-style2 {
            text-align: center;
        }
        .auto-style3 {
            height: 30px;
            width: 706px;
            text-align: right;
        }
        .auto-style4 {
            text-align: left;
        }
        .auto-style5 {
            height: 30px;
            text-align: left;
        }
        .auto-style6 {
            width: 50%;
            padding-right: 25px;
        }
        .auto-style7 {
            width: 50%;
            text-align: right;
            padding-right: 150px;
        }
    </style>
</head>
<body>
    <form id="form1" defaultbutton="btnSubmit" defaultfocus="txtName" runat="server">
        <div class="auto-style2">
            <uc1:UIHeader ID="UIHeader1" runat="server" />
        <table class="auto-style1">
            <tr>
                <td colspan="2">
                    <h1 class="auto-style2">User Information</h1>
                </td>
            </tr>
            <tr>
                <td class="auto-style6">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style7">Name</td>
                <td class="auto-style4">
                    <asp:TextBox ID="txtName" runat="server" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" Display="Dynamic" ErrorMessage="The name field is required" ForeColor="Red">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style7">DOB</td>
                <td class="auto-style4">
                    <asp:TextBox ID="txtDate" runat="server" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvDate" runat="server" ControlToValidate="txtDate" Display="Dynamic" EnableTheming="True" ErrorMessage="The date field is required" ForeColor="Red">*</asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="cvDOB" runat="server" ControlToValidate="txtDate" Display="Dynamic" ErrorMessage="The date should be in format: 12/12/1900" ForeColor="Red" Operator="DataTypeCheck" Type="Date">*</asp:CompareValidator>
                    <asp:CompareValidator ID="cvDOB18" runat="server" ControlToValidate="txtDate" ErrorMessage="Must be 18 y.o." ForeColor="#CC0000" Operator="LessThan" Type="Date" ValueToCompare="1/1/2000">*</asp:CompareValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style7">Email</td>
                <td class="auto-style4">
                    <asp:TextBox ID="txtEmail" runat="server" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail" Display="Dynamic" ErrorMessage="The email field is required" ForeColor="Red">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="regexValidator" runat="server" ControlToValidate="txtEmail" Display="Dynamic" ErrorMessage="Please put email in the proper format" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style7">Province</td>
                <td class="auto-style5">
                    <asp:DropDownList ID="ddlProvince" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlProvince_SelectedIndexChanged" Width="200px">
                        <asp:ListItem>Please select Province</asp:ListItem>
                        <asp:ListItem>QC</asp:ListItem>
                        <asp:ListItem>BC</asp:ListItem>
                        <asp:ListItem>ON</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvProvince" runat="server" ControlToValidate="ddlProvince" Display="Dynamic" ErrorMessage="The province field is required" ForeColor="Red" InitialValue="Please select Province">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style7">City</td>
                <td class="auto-style4">
                    <asp:DropDownList ID="ddlCity" runat="server" Width="200px">
                        <asp:ListItem>Please select City</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvCity" runat="server" ControlToValidate="ddlCity" Display="Dynamic" ErrorMessage="The city field is required" ForeColor="Red" InitialValue="Please select City">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                    <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="Submit" />
                </td>
            </tr>
            <tr>
                <td colspan="2" class="auto-style4">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
                </td>
            </tr>
        </table>
        <br />
        <br />
        <asp:Label ID="lblResults" runat="server" Font-Bold="True" Font-Italic="True"></asp:Label>
        <br />
        <br />
        <br />
        <asp:ListBox ID="lbOne" runat="server" Height="63px" Width="587px" AutoPostBack="True" OnSelectedIndexChanged="lbOne_SelectedIndexChanged"></asp:ListBox>
        <input type="hidden" id="recordCounter" value="0" runat="server">
            <uc2:UIFooter ID="UIFooter1" runat="server" />
        </div>
    </form>
    
</body>
</html>
