﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebPageLifeCycle
{
    public partial class _3___ViewState : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                txtCount.Text = "0";
            }
        }

        protected void btnOne_Click(object sender, EventArgs e)
        {
            if (ViewState["Clicks"]==null)
            {
                ViewState["Clicks"] = 1;
            }

            txtCount.Text = ViewState["Clicks"].ToString();
            ViewState["Clicks"] = (int)ViewState["Clicks"] + 1;
        }

        protected void btnIncControl_Click(object sender, EventArgs e)
        {
            int count = int.Parse(txtCount.Text) + 1;
            txtCount.Text = count.ToString();
        }
    }
}