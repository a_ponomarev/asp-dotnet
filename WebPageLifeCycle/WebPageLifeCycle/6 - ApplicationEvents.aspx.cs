﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebPageLifeCycle
{
    public partial class _6___ApplicationEvents : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Write("Number of apps = " + Application["NumberOfApplications"]);
            Response.Write("<br/>");
            Response.Write("Number of sessions = " + Application["NumberOfSessions"]);
        }
    }
}