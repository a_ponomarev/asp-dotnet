﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="9 - Navigation.aspx.cs" Inherits="WebPageLifeCycle._9___Navigation" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            ASP HyperLink Control<br />
            <br />
            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/9 - Navigation (target).aspx">Internal Link</asp:HyperLink>
            <br />
            <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="http://google.com">External Link</asp:HyperLink>
            <br />
            <br />
            Response.Redirect<br />
            <br />
            <asp:Button ID="btnRedirectInternal" runat="server" OnClick="btnRedirectInternal_Click" Text="Redirect Internal" />
            <asp:Button ID="btnRedirectExternal" runat="server" OnClick="btnRedirectExternal_Click" Text="Redirect External" />
            <br />
            <br />
            Server.Transfer<br />
            <br />
            <asp:Button ID="btnTransferRedirectInternal" runat="server" OnClick="btnTransferRedirectInternal_Click" Text="Transfer Redirect Internal" />
            <asp:Button ID="btnTransferRedirectExternal" runat="server" OnClick="btnTransferRedirectExternal_Click" Text="Transfer Redirect External" />
            <br />
            <br />
            <hr />
            <br />
            Username:&nbsp;&nbsp;&nbsp; <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
            <br />
            Email:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="btnSend" runat="server" OnClick="btnSend_Click" Text="Server.Transfer (Target 2)" />
            <br />
            <br />
            <asp:Button ID="btnServerExecute" runat="server" OnClick="btnServerExecute_Click" Text="Server.Execute (Target 2)" />
            <br />
            <br />
            <asp:Label ID="lblResult" runat="server"></asp:Label>
            <br />
            <br />
            <hr />
        </div>
        <br />
        <asp:Button ID="btnCrossPage" runat="server" PostBackUrl="~/9 - CrossPagePostBack (target).aspx" Text="Cross Page Post Back" />
        <br />
        <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/9 - CrossPagePostBack (target).aspx">Cross Page Target</asp:HyperLink>
    </form>
</body>
</html>
