﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace WebPageLifeCycle
{
    public class Global : System.Web.HttpApplication
    {
        // track number of running apps & sessions
        protected void Application_Start(object sender, EventArgs e)
        {
            Application["NumberOfApplications"] = 0;
            Application["NumberOfSessions"] = 0;

            Application["NumberOfApplications"] = (int)Application["NumberOfApplications"] + 1;
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            Application["NumberOfSessions"] = (int)Application["NumberOfSessions"] + 1;
        }

        protected void Session_End(object sender, EventArgs e)
        {
            Application["NumberOfSessions"] = (int)Application["NumberOfSessions"] - 1;
        }
    }
}