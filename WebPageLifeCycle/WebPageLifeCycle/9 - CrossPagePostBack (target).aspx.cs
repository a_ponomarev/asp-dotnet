﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebPageLifeCycle
{
    public partial class _10___CrossPagePostBack : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Page previous = Page.PreviousPage;

            if (previous != null && previous.IsCrossPagePostBack)
            {
                Response.Write("This is a cross page post back target");
            } else
            {
                Response.Write("You have reached this page using the wrong means");
            }
        }
    }
}