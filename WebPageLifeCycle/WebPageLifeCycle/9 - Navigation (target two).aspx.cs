﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebPageLifeCycle
{
    public partial class _9___Navigation__target_two_ : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Method 1

            //Page prevPage = Page.PreviousPage;

            //if (prevPage != null)
            //{
            //    lblName.Text = ((TextBox)prevPage.FindControl("txtName")).Text;
            //    lblEmail.Text = ((TextBox)prevPage.FindControl("txtEmail")).Text;
            //} else
            //{
            //    lblName.Text = "You didn't navigate from a proper page";
            //}

            // Method 2

            //lblName.Text = Request.Form["txtName"];
            //lblName.Text = Request.Form["txtEmail"];

            // Method 3 - StronglyTyped

            _9___Navigation prevPage = (_9___Navigation) Page.PreviousPage;

            if (prevPage != null)
            {
                lblName.Text = prevPage.name;
                lblEmail.Text = prevPage.email;
            }
        }
    }
}