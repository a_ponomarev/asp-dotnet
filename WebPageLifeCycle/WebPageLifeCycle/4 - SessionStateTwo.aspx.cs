﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebPageLifeCycle
{
    public partial class _4___SessionStateTwo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Web.config регулирует время таймаута хранения сессий

            if (Session["visits"] == null)
            {
                Session["visits"] = 1;
                lblVisits.Text = "This is your 1 visit";

            }
            else
            {
                Session["visits"] = (int)Session["visits"] + 1;
                lblVisits.Text = "Welcome back .. visit #" + Session["visits"];
            }
        }
    }
}