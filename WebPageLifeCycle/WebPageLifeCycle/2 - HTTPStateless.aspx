﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="2 - HTTPStateless.aspx.cs" Inherits="WebPageLifeCycle._2___HTTPStateless" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            My Count:
            <asp:TextBox ID="txtCount" runat="server"></asp:TextBox>
            <asp:Button ID="btnOne" runat="server" OnClick="btnOne_Click" Text="Increment" />
            <hr />
        </div>
    </form>
</body>
</html>
