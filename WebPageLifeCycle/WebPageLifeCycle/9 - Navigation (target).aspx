﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="9 - Navigation (target).aspx.cs" Inherits="WebPageLifeCycle._9___Navigation__target_" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            This is my target page.
        </div>
    </form>
</body>
</html>
