﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebPageLifeCycle
{
    public partial class SequenceOfEvents : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string eventMsg = "Page load is called";
            if (Page.IsPostBack)
            {
                eventMsg += "-Postback";
            }

            blEventLog.Items.Add(eventMsg);

        }

        protected void Page_Init(object sender, EventArgs e)
        {
            blEventLog.Items.Add("Page initialize is called");
            addDynamicContent();
        }

        private void addDynamicContent()
        {
            var dynamicBtn = new Button();

            dynamicBtn.Text = "Click me";
            dynamicBtn.ID = "btnDynamic";
            dynamicBtn.Click += this.btnDynamic_Click;
            dynamicBtn.Click += this.btnDynamic_Click2;

            var dynamicLbl = new Label();
            dynamicLbl.ID = "lblDynamic";

            phDynamicContent.Controls.Add(dynamicBtn);
            phDynamicContent.Controls.Add(dynamicLbl);

        }
        protected void btnDynamic_Click(Object sender, EventArgs e)
        {
            var tempLabel = (Label)phDynamicContent.FindControl("lblDynamic");
            tempLabel.Text += " Yaay ";
        }

        protected void btnDynamic_Click2(Object sender, EventArgs e)
        {
            //blEventLog.Items.Add("Dynamic Button clicked");

            var tempLabel = (Label)phDynamicContent.FindControl("lblDynamic");
            tempLabel.Text += " Woow ";
        }
    }
}