﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebPageLifeCycle
{
    public partial class _5___ApplicationState : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnOne_Click(object sender, EventArgs e)
        {
            if (Application["Clicks"] == null)
            {
                Application["Clicks"] = 1;
            }

            txtCount.Text = Application["Clicks"].ToString();
            Application["Clicks"] = (int)Application["Clicks"] + 1;
        }
    }
}