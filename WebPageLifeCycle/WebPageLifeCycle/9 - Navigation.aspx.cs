﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebPageLifeCycle
{
    public partial class _9___Navigation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnRedirectInternal_Click(object sender, EventArgs e)
        {
            // add any logic...
            Response.Redirect("~/9 - Navigation (target).aspx");
        }

        protected void btnRedirectExternal_Click(object sender, EventArgs e)
        {
            // add any logic...
            Response.Redirect("http://google.com");
        }

        protected void btnTransferRedirectInternal_Click(object sender, EventArgs e)
        {
            Server.Transfer("~/9 - Navigation (target).aspx");
        }

        protected void btnTransferRedirectExternal_Click(object sender, EventArgs e)
        {
            Server.Transfer("http://google.com");
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            Server.Transfer("~/9 - Navigation (target two).aspx");
            lblResult.Text = "You have succesfully input data";
            txtName.Text = string.Empty;
            txtEmail.Text = string.Empty;
        }

        public string name
        {

            get
            {
                return txtName.Text;
            }
        }

        public string email
        {

            get
            {
                return txtEmail.Text;
            }
        }

        protected void btnServerExecute_Click(object sender, EventArgs e)
        {
            Server.Execute("~/9 - Navigation (target two).aspx");
            lblResult.Text = "You have succesfully input data";
            txtName.Text = string.Empty;
            txtEmail.Text = string.Empty;
        }
    }
}