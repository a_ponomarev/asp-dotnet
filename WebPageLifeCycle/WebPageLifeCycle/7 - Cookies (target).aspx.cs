﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebPageLifeCycle
{
    public partial class _7___Cookies__target_ : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpCookie cookie = Request.Cookies["UserInfo"];

            if (cookie != null)
            {
                lblName.Text = cookie["name"];
                lblEmail.Text = cookie["email"];
            } else
            {
                lblError.Text = "You have arrived at this page illegaly";
            }
        }
    }
}