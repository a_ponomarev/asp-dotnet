﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="8 - QueryString (target).aspx.cs" Inherits="WebPageLifeCycle._8___QueryString__target_" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="lblName" runat="server"></asp:Label>
            <br />
            <asp:Label ID="lblEmail" runat="server"></asp:Label>
            <br />
            <asp:Label ID="lblError" runat="server" Font-Bold="True" ForeColor="Maroon"></asp:Label>
        </div>
    </form>
</body>
</html>
