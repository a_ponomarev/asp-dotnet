﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="3 - ViewState.aspx.cs" Inherits="WebPageLifeCycle._3___ViewState" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            My Count:
            <asp:TextBox ID="txtCount" runat="server"></asp:TextBox>
            <asp:Button ID="btnOne" runat="server" OnClick="btnOne_Click" Text="Increment" />
            <asp:Button ID="btnIncControl" runat="server" OnClick="btnIncControl_Click" Text="Increment with Control" />
            <br />
            HTML Control:
            <input id="Text1" type="text" /><asp:Button ID="btnTwo" runat="server" Text="Dummy Post Back" />
        </div>
    </form>
</body>
</html>
