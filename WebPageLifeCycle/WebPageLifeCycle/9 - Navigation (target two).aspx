﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="9 - Navigation (target two).aspx.cs" Inherits="WebPageLifeCycle._9___Navigation__target_two_" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            The nav target part 2<br />
            <br />
            <asp:Label ID="lblName" runat="server"></asp:Label>
            <br />
            <asp:Label ID="lblEmail" runat="server"></asp:Label>

        </div>
    </form>
</body>
</html>
