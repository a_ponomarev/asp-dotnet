﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="1 - SequenceOfEvents.aspx.cs" Inherits="WebPageLifeCycle.SequenceOfEvents" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:BulletedList ID="blEventLog" runat="server">
            </asp:BulletedList>
        </div>
        <asp:Button ID="btnSubmit" runat="server" Height="26px" Text="Send" Width="46px" />
        <br />
        <hr />
        <br />
        This is static upper text <br />
        <br />
        <asp:PlaceHolder id="phDynamicContent" runat="server" />
        <br />
        <br />
        This is static lower text

    </form>
        <hr />
        <p>
            &nbsp;</p>
</body>
</html>
