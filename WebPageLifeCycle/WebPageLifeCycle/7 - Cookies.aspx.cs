﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebPageLifeCycle
{
    public partial class _7___Cookies : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            // get info from the form & save it into a cookie

            HttpCookie cookie = new HttpCookie("UserInfo");
            cookie["name"] = txtName.Text;
            cookie["email"] = txtEmail.Text;

            Response.Cookies.Add(cookie);

            Response.Redirect("~/7 - Cookies (target).aspx");


        }
    }
}