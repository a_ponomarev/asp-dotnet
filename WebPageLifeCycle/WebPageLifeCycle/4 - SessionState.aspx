﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="4 - SessionState.aspx.cs" Inherits="WebPageLifeCycle._4___SessionState" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            My Count:
            <asp:TextBox ID="txtCount" runat="server"></asp:TextBox>
            <asp:Button ID="btnOne" runat="server" OnClick="btnOne_Click" Text="Increment" />
            <br />
            <br />
            <br />
            <asp:Label ID="lblVisits" runat="server"></asp:Label>
            <br />
            <br />
            <br />
            <hr />
        </div>
    </form>
</body>
</html>
