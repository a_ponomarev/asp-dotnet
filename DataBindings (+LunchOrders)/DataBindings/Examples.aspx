﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Examples.aspx.cs" Inherits="DataBindings.Examples" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="css/styles.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>Databinding Demo</h1>
            <asp:BulletedList ID="blSearchEngines" runat="server" DataTextField="Name" DataValueField="val" DisplayMode="HyperLink">
            </asp:BulletedList>
            <br />
            <br />
            Part 1 - DropDown List<br />
            <br />
            Q: Is ASP .NET easy?<br />
            <br />
            <asp:DropDownList ID="ddlAnswers" runat="server" AutoPostBack="True" Height="16px" OnSelectedIndexChanged="ddlAnswers_SelectedIndexChanged" Width="166px">
            </asp:DropDownList>
            <br />
            <br />
            <asp:Label ID="lblAnswer" runat="server" Font-Bold="True" Font-Italic="True" ForeColor="Blue"></asp:Label>
            <br />
            <hr />
        </div>
        <p>
            Part 2 - List Box</p>
        <asp:ListBox ID="lbCity" runat="server" AutoPostBack="True" DataTextField="Name" DataValueField="ID" Height="93px" OnSelectedIndexChanged="lbCity_SelectedIndexChanged" Width="123px"></asp:ListBox>
        <br />
        <br />
            <asp:Label ID="lblAnswerCity" runat="server" Font-Bold="True" Font-Italic="True" ForeColor="Blue"></asp:Label>
    </form>
</body>
</html>
