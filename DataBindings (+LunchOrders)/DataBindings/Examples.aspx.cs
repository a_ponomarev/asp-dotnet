﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DataBindings
{
    public partial class Examples : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                // Dropdown list answers
                var answers = new string[] { "Yes", "No", "Maybe", "Naah" };

                // 1 - data source 2 - binding

                ddlAnswers.DataSource = answers;
                ddlAnswers.DataBind();

                // Listbox binding: array of cities (ID & Name)

                var cities = new[]
                {
                new{ID=1, Name = "Montreal"},
                new{ID=2, Name = "Ottawa"},
                new{ID=3, Name = "London"},
                new{ID=4, Name = "NY"},
                new{ID=5, Name = "Paris"}

                };

                // 1 - data source 2 - binding

                lbCity.DataSource = cities;
                lbCity.DataBind();

                // Bulleted List

                var searchEngines = new[]
                {
                new{Name = "Google", val="http://google.com"},
                new{Name = "Bing", val="http://bing.com"},
                new{Name = "Yandex", val="http://ya.ru"},
                new{Name = "Yahoo", val="http://yahoo.com"},
                

                };

                // 1 - data source 2 - binding

                blSearchEngines.DataSource = searchEngines;
                blSearchEngines.DataBind();

            }
        }

        protected void ddlAnswers_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblAnswer.Text = "Your answer is " + ddlAnswers.SelectedValue;


        }

        protected void lbCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblAnswerCity.Text = "You have selected: " + lbCity.SelectedItem.ToString() + " ID #" + lbCity.SelectedValue;
        }
    }
}