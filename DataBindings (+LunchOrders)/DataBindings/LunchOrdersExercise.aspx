﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LunchOrdersExercise.aspx.cs" Inherits="DataBindings.LunchOrdersExercise" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            text-align: center;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table class="auto-style1">
                <tr>
                    <td colspan="2">
                        <h1 class="auto-style2">Lunch Orders</h1>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Name:</td>
                    <td>
                        <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Order number:</td>
                    <td>
                        <asp:TextBox ID="txtOrderID" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>City:</td>
                    <td>
                        <asp:ListBox ID="lbCities" runat="server" DataTextField="Name" DataValueField="ID" Height="59px" Width="129px"></asp:ListBox>
                    </td>
                </tr>
                <tr>
                    <td>Gender:</td>
                    <td>
                        <asp:DropDownList ID="ddlGender" runat="server" Height="16px" Width="128px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>Meal Options:</td>
                    <td>
                        <asp:CheckBoxList ID="cblMeal" runat="server" DataTextField="Name" DataValueField="ID">
                        </asp:CheckBoxList>
                    </td>
                </tr>
                <tr>
                    <td>Payment Method:</td>
                    <td>
                        <asp:RadioButtonList ID="rblPayment" runat="server" DataTextField="Name" DataValueField="ID">
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2" colspan="2">
                        <br />
                        <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="Submit" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2" colspan="2">
                        <br />
                        <br />
                        <asp:ListBox ID="lbList" runat="server" Height="89px" Width="453px"></asp:ListBox>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
