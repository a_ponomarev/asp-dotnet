﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DataBindings
{
    public partial class LunchOrdersExercise : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                // Listbox binding: array of cities (ID & Name)

                var cities = new[]
                {
                new{ID=1, Name = "Montreal"},
                new{ID=2, Name = "Ottawa"},
                new{ID=3, Name = "London"},
                new{ID=4, Name = "NY"},
                new{ID=5, Name = "Paris"}

                };

                // 1 - data source 2 - binding

                lbCities.DataSource = cities;
                lbCities.DataBind();

                // Dropdown list answers
                var gender = new string[] { "Male", "Female", "N/A", "Binary" };

                // 1 - data source 2 - binding

                ddlGender.DataSource = gender;
                ddlGender.DataBind();

                // Checkbox list binding: array of meals (ID & Text)

                var meals = new[]
                {
                new{ID=1, Name = "Burger"},
                new{ID=2, Name = "Salad"},
                new{ID=3, Name = "Coke"},
                new{ID=4, Name = "Steak"},
                };

                cblMeal.DataSource = meals;
                cblMeal.DataBind();

                // RadioButton list binding: array of payments (ID & Text)

                var methods = new[]
                {
                new{ID=1, Name = "Cash"},
                new{ID=2, Name = "Credit Card"},
                new{ID=3, Name = "Android Pay"},
                new{ID=4, Name = "Apple Pay"},
                };

                rblPayment.DataSource = methods;
                rblPayment.DataBind();
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            String name = txtName.Text;
            String id = txtOrderID.Text;
            String gender = ddlGender.SelectedItem.ToString();
            String city = lbCities.SelectedItem.ToString();

            String payment = rblPayment.SelectedItem.ToString();
            String order = name + " | " + id + " | " + city + " | " + payment + " | Meals: ";

            string tmp = "";

            for (int i = 0; i < cblMeal.Items.Count; i++)
            {

                if (cblMeal.Items[i].Selected)
                {
                    tmp += ((tmp == "") ? "" : " / ") + cblMeal.Items[i].Text;
                }

            }

            order += tmp;

            lbList.Items.Add(order);
        }
    }
}