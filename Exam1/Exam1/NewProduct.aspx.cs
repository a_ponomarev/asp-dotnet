﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Exam1
{
    public partial class NewProduct : System.Web.UI.Page
    {
        private string connString = ConfigurationManager.ConnectionStrings["NorthwindConnectionString"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {
            UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            string ProductName = txtProductName.Text;
            string QuantityPerUnit = txtQuantityPerUnit.Text;
            string UnitPrice = txtUnitPrice.Text;
            string UnitsInStock = txtUnitsInstock.Text;
            string UnitsOnOrder = txtUnitsOnOrder.Text;
            string OrderLevel = txtOrderLevel.Text;
            bool Discontinued = chbDiscontinued.Checked;
            int SupplierID = int.Parse(Request.QueryString["supplierid"]);
            int Category = int.Parse(ddlCategories.SelectedValue);
            
            // 1 - SQL connection
            SqlConnection con = null;

            try
            {
                con = new SqlConnection(connString);
                string sqlQuery = "insert into Products values('" + ProductName + "','" + SupplierID + "','" + Category +"','" + QuantityPerUnit +"','" + UnitPrice +"','" + UnitsInStock + "','" + UnitsOnOrder + "','" + OrderLevel +"','" + Discontinued + "')";
                SqlCommand cmd = new SqlCommand(sqlQuery, con);

                con.Open();

                cmd.ExecuteNonQuery();

                lblLog.Text = "Shipper added succesfully!";
                btnAdd.Enabled = false;
                hlHome.Visible = true;
            }
            catch (Exception)
            {

            }
            finally
            {
                con.Close();
            }
        }
    }
}