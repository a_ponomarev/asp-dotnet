﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Exam1
{
    public partial class SupplierSearch : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void ddlSuppliers_SelectedIndexChanged(object sender, EventArgs e)
        {
            Response.Redirect("~/SelectedSuppliers.aspx?supplierid=" + Server.UrlEncode(ddlSuppliers.SelectedValue));
        }

        protected void gvSuppliers_SelectedIndexChanged(object sender, EventArgs e)
        {
            Response.Redirect("~/SelectedSuppliers.aspx?supplierid=" + Server.UrlEncode(gvSuppliers.SelectedValue.ToString()));
        }

        
    }
}