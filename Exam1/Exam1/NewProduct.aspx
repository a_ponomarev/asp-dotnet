﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NewProduct.aspx.cs" Inherits="Exam1.NewProduct" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style2 {
            width: 100%;
        }

        .auto-style3 {
            text-align: center;
        }

        .auto-style5 {
            text-align: center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <br />
    <br />
    <table class="auto-style2">
        <tr>
            <td class="auto-style3" colspan="2">
                <h1 class="auto-style5">Add New Product</h1>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>Product Name:</td>
            <td>
                <asp:TextBox ID="txtProductName" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rvProductName" runat="server" ControlToValidate="txtProductName" Display="Dynamic" ErrorMessage="Product Name is required"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>Quantity Per Unit:</td>
            <td>
                <asp:TextBox ID="txtQuantityPerUnit" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvQuantityPerUnit" runat="server" ControlToValidate="txtQuantityPerUnit" Display="Dynamic" ErrorMessage="Quantity is required"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>Unit Price:</td>
            <td>
                <asp:TextBox ID="txtUnitPrice" runat="server"></asp:TextBox>
                <asp:RangeValidator ID="rvUnitPrice" runat="server" ControlToValidate="txtUnitPrice" Display="Dynamic" ErrorMessage="The value should be double" MaximumValue="10000.0" MinimumValue="0.0" Type="Double"></asp:RangeValidator>
            </td>
        </tr>
        <tr>
            <td>Units In stock:</td>
            <td>
                <asp:TextBox ID="txtUnitsInstock" runat="server"></asp:TextBox>
                <asp:RangeValidator ID="rvUnitInSt" runat="server" ControlToValidate="txtUnitsInstock" Display="Dynamic" ErrorMessage="The value should be int" MaximumValue="10000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
            </td>
        </tr>
        <tr>
            <td>Units On Order:</td>
            <td>
                <asp:TextBox ID="txtUnitsOnOrder" runat="server"></asp:TextBox>
                <asp:RangeValidator ID="rvUnitOnOrder" runat="server" ControlToValidate="txtUnitsOnOrder" Display="Dynamic" ErrorMessage="The value should be int" MaximumValue="10000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
            </td>
        </tr>
        <tr>
            <td>Order Level:</td>
            <td>
                <asp:TextBox ID="txtOrderLevel" runat="server"></asp:TextBox>
                <asp:RangeValidator ID="rvOrderLevel" runat="server" ControlToValidate="txtOrderLevel" Display="Dynamic" ErrorMessage="The value should be int" MaximumValue="10000" MinimumValue="0" Type="Integer"></asp:RangeValidator>
            </td>
        </tr>
        <tr>
            <td>Discontinued:</td>
            <td>
                <asp:CheckBox ID="chbDiscontinued" runat="server" />
            </td>
        </tr>
        <tr>
            <td>Category ID:</td>
            <td>
                <asp:DropDownList ID="ddlCategories" runat="server" DataSourceID="SqlDSCategoryID" DataTextField="CategoryName" DataValueField="CategoryID">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDSCategoryID" runat="server" ConnectionString="<%$ ConnectionStrings:NorthwindConnectionString %>" SelectCommand="SELECT [CategoryID], [CategoryName] FROM [Categories]"></asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblLog" runat="server"></asp:Label>
            </td>
            <td>
                <asp:Button ID="btnAdd" runat="server" Text="Add New Product" OnClick="btnAdd_Click" />
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <br />
                <br />
                <asp:HyperLink ID="hlHome" runat="server" NavigateUrl="~/SupplierSearch.aspx" Visible="False">Go to Supplier Search Form</asp:HyperLink>
            </td>
        </tr>
    </table>
</asp:Content>
