﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SupplierSearch.aspx.cs" Inherits="Exam1.SupplierSearch1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <style type="text/css">
        .auto-style5 {
            text-align: center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContent" runat="server">
    <h1 class="auto-style5">&nbsp;</h1>
    <h1 class="auto-style5">Supplier Search Form</h1><br />
<br />
                    <asp:DropDownList ID="ddlSuppliers" runat="server" AutoPostBack="True" AppendDataBoundItems="True" DataSourceID="ObjDSGetAllSuppliers" DataTextField="CompanyName" DataValueField="SupplierID" OnSelectedIndexChanged="ddlSuppliers_SelectedIndexChanged">
                        <asp:ListItem Value="" Text="Please select a supplier" />
                    </asp:DropDownList>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="txtSearchByCity" runat="server" Width="300px"></asp:TextBox>
                    <asp:Button ID="btnSearchByCity" runat="server" Text="Search By CIty" />
                <br />
<%--<form id="form1" runat="server">--%>
            <asp:ObjectDataSource ID="ObjDSGetAllSuppliers" runat="server" SelectMethod="getAllSupliers" TypeName="Exam1.DAL.DAL_Northwind"></asp:ObjectDataSource>
        <div>
            <br />
            <br />
            <div>
                <asp:GridView ID="gvSuppliers" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="SupplierID" DataSourceID="ObjDSGetSuppliersFromCity" ForeColor="#333333" GridLines="None" OnSelectedIndexChanged="gvSuppliers_SelectedIndexChanged">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:CommandField ShowSelectButton="True" />
                        <asp:BoundField DataField="SupplierID" HeaderText="SupplierID" SortExpression="SupplierID" />
                        <asp:BoundField DataField="CompanyName" HeaderText="CompanyName" SortExpression="CompanyName" />
                        <asp:BoundField DataField="ContactName" HeaderText="ContactName" SortExpression="ContactName" />
                        <asp:BoundField DataField="ContactTitle" HeaderText="ContactTitle" SortExpression="ContactTitle" />
                        <asp:BoundField DataField="Address" HeaderText="Address" SortExpression="Address" />
                        <asp:BoundField DataField="City" HeaderText="City" SortExpression="City" />
                        <asp:BoundField DataField="Region" HeaderText="Region" SortExpression="Region" />
                        <asp:BoundField DataField="PostalCode" HeaderText="PostalCode" SortExpression="PostalCode" />
                        <asp:BoundField DataField="Country" HeaderText="Country" SortExpression="Country" />
                        <asp:BoundField DataField="Phone" HeaderText="Phone" SortExpression="Phone" />
                        <asp:BoundField DataField="Fax" HeaderText="Fax" SortExpression="Fax" />
                        <asp:BoundField DataField="HomePage" HeaderText="HomePage" SortExpression="HomePage" />
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </div>
            <asp:ObjectDataSource ID="ObjDSGetSuppliersFromCity" runat="server" SelectMethod="getAllSupliersFromASpecificCity" TypeName="Exam1.DAL.DAL_Northwind">
                <SelectParameters>
                    <asp:ControlParameter ControlID="txtSearchByCity" DefaultValue="null" Name="city" PropertyName="Text" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </div>
    <%--</form>--%>
    <script>
        $('#mainContent_btnSearchByCity').click(function () {
            $('#mainContent_ddlSuppliers').prop('selectedIndex', 0);
        });
    </script>
</asp:Content>
