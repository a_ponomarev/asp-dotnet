﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Exam1.DAL
{
    public class DAL_Northwind
    {
        // get all suppliers from northwind to get them after into the dropdown list
        public List<Supplier> getAllSupliers()
        {
            // create context for accesing classes that we created through LINQ to SQL
            using (NorthwindDataContext context = new NorthwindDataContext())
            {
                return (from s in context.Suppliers
                        select s).ToList();
            }
                
        }

        // get all suppliers from a specific city
        public List<Supplier> getAllSupliersFromASpecificCity(string city)
        {
            // create context for accesing classes that we created through LINQ to SQL
            using (NorthwindDataContext context = new NorthwindDataContext())
            {
                List<Supplier> suppliers = 
                (from s in context.Suppliers
                 where s.City.StartsWith(city) || s.City.EndsWith(city) // to implement LIKE equivalent
                 select s).ToList();

                return suppliers;
            }

        }
    }
}